export default {
  admin: {
    dist: '../webapp/static/admin',
    browserify: {entries: ['./scripts/admin/index.js'] },
    stylesheets: [
      './stylesheets/admin/vendor/bootstrap.css',
      './stylesheets/admin/theme/base.css',
      './stylesheets/admin/theme/skin.css',
      './stylesheets/admin/application.css',
      './node_modules/v-accordion/dist/v-accordion.css',
      './node_modules/toastr/build/toastr.css'
    ],

    watch: {
      js: ['./scripts/admin/**/*', './scripts/builder/**/*'],
      css: './stylesheets/admin/**/*'
    }
  },

  user: {
    dist: '../webapp/static/user',
    browserify: {entries: './scripts/user/index.js'},
    stylesheets: [
      './stylesheets/user/vendor/bootstrap.css',
      './stylesheets/user/theme/main.css'
    ],

    watch: {
      js: './scripts/user/**/*',
      css: './stylesheets/user/**/*'
    }
  }

};