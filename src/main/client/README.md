# Setup

Install [node](https://nodejs.org/en/) & depenedencies

# Building frontend

You will need to build every time you pull frontend changes:

    > cd src/main/client
    > npm run build            # build codebase
    > npm run bundle           # build for production

Files are built to `src/main/webapp/static`.

NOTE: First time running these will take some time. Make a coffee.

# Frontend development

Run `gulp dev:admin|user` depending on what interface you want then gulp with watch files/ rebuild. Nothing fancy.

Install your dependencies with npm and include them in index.js (the only file run though the build). There is no enforced linting - so please follow codebase conventions.


# For my future self ;) - (cheers bro ^^)

From root:

  * db: (OSX mysql.server start) (ubuntu sudo service mysql start)
  * server: mvn spring-boot:run (from root)
  * sql: mysql -u root -p       - password: admin
  * drop db: DROP DATABASE assetshare
  * recreate db: copy paste scripts.sql - doesnt matter what one
  * go here: http://assetshare.com.au:8080/manage or http://client.assetshare.com.au:8080/

Host:
  * sudo vi /etc/hosts # 127.0.0.1 clientname.assetshare.com.au/

Logins:

  * Sam (System Admin) | assetshare101@gmail.com // test
  * Provider
    - John | test1@gmail.com // test
    - Martin | test2@gmail.com // test
  * Consumer
    - Joe | test3@gmail.com // test
    - Fart o.O | test4@gmail.com // test
