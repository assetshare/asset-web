import formly from 'angular-formly';
import formlyBootstrap from 'angular-formly-templates-bootstrap';
import vAccordion from 'v-accordion';
import ngAnimate from 'angular-animate';
import lrUpload from 'angular-upload';


import FormBuilder from '../builder/FormBuilder';

angular.module('Forms', ['Common', formly, formlyBootstrap, vAccordion, ngAnimate, 'lr.upload'])

.constant('FORM_TYPES', ['REGISTER', 'BOOKING'])

.controller('formBuilderCtrl', [
  '$http',
  '$timeout',
  'API_BASE',
  'FORM_TYPES',
  '$scope',
  function ($http, $timeout, API, TYPES, $scope) {
    this.form = {};
    this.addType = 'input'; // Default field type to add
    this.fields = [];

    let formBuilder = new FormBuilder();

    $timeout(() => { // Waits for ngInit to set form.type - not ideal, but yolo
      const url = `${API}/forms/${this.type.toLowerCase()}/load`;
      
      $http.get(url).then(resp => {
        if (resp.data) {
          this.form = resp.data;
          this.fields = this.form.config || this.fields;
        }

        formBuilder = new FormBuilder(this.fields);
      });
    });

    this.saveForm = () => {
      const postData = angular.copy(this.form);
      postData.config = this.fields;
      postData.type = this.type;

      $http.post(`${API}/formbuilder`, postData).then(resp => {
        toastr.success('Form successfully saved');
        this.form = resp.data;
        this.fields = this.form.config;        
      });
    };

    this.showFormly = () => {
      this.showPreview = true;
      this.formlyFields = formBuilder.getFieldsCopy();
    };

    this.addField = type => {
      formBuilder.add(type);
      $timeout(() => {
        const id = _.last(this.fields).id;
        $scope.accordion.expand(id);
        document.getElementById(id).scrollIntoView(); // Sue me
      }, 300);
    };

    this.remove = field => formBuilder.remove(field, true);
    this.move = (field, dir) => formBuilder.move(field, dir);

    this.autoPlaceholder = (field) => {
      field.templateOptions.placeholder = `enter ${field.templateOptions.label}`.toLowerCase();
    };
  }
])

// todo move him out
.run(require('../builder/file_upload.js'))

;
