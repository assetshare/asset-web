import jQuery from 'jquery';
import lodash from 'lodash';
import tether from 'tether';
import toastr from 'toastr';
import angular from 'angular';


import ResponseParser from '../builder/ResponseParser.js';

window.jQuery = jQuery;
window._ = lodash;
window.Tether = tether;
window.bootstrap = require('bootstrap');
window.toastr = toastr;
window.angular = angular;

require('./common_module.js');
require('./forms_module.js');

angular.module('AssetAdmin', ['Forms', 'Common'])

.controller('bookingCtrl', [
  function () {
    const parser = new ResponseParser(
      window.APP.pageData.fields,
      window.APP.pageData.response
    );

    this.additionalDetails = parser.merge();
  }
])