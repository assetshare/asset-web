import jQuery from 'jquery';
import lodash from 'lodash';
import tether from 'tether';
import angular from 'angular';

import formly from 'angular-formly';
import formlyBootstrap from 'angular-formly-templates-bootstrap';
import lrUpload from 'angular-upload';

import ResponseParser from '../builder/ResponseParser.js';

window.jQuery = jQuery;
window.$ = jQuery;
window._ = lodash;
window.Tether = tether;
window.bootstrap = require('bootstrap');
window.angular = angular;

angular.module('AssetPortal', [formly,formlyBootstrap,'lr.upload'])

.run(require('../builder/file_upload.js'))

.controller('bookingCtrl', [
  '$http',
  '$timeout',
  function ($http, $timeout) {
    const parser = new ResponseParser(
      window.APP.pageData.fields,
      window.APP.pageData.responses
    );

    this.additionalDetails = parser.merge();

    $http.get('/forms/booking/load').then(resp => {
      this.form = resp.data;
      this.form.config.forEach(i => i.key = i.id);
    });

    this.onAfterUpload = (resp, model, key) => {
      model[key] = resp.data;
    };

    this.submit = (config, assetId) => {
      const data = _.extend({ config }, this.defaultFields);

      $http.post(`/assets/${assetId}/book`, {booking: data}).then(resp => {
        console.log('do something')
      });
    };

    this.openBookingForm = () => {
      this.showBookingForm = true;
      $timeout(() => {
        window.scrollTo(0, $("#book").offset().top- 100);      
      });
    };
  }
])

.controller('registerCtrl', [
  '$http',
  function ($http) {
    this.defaultFields = {};

    $http.get('/forms/register/load').then(resp => {
      this.form = resp.data;
      this.form.config.forEach(i => i.key = i.id);
    });

    this.onAfterUpload = (resp, model, key) => {
      model[key] = resp.data;
    };

    this.submit = (config) => {
      const data = _.extend({ config }, this.defaultFields);

      $http.post('/assets/register' ,{register: data}).then(resp => {
        window.location = `/assets/${resp.data.register.id}/book`;
      });
    };
  }
])

;