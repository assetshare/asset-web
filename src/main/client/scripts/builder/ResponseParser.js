export default class ResponseParser {
  constructor(fields, responses) {
    this.fields = fields;
    this.responses = responses;
  }

  merge() {
    const fields = angular.copy(this.fields);

    fields.forEach(field => {
      field.response = this.responses[field.id];
      field.label = field.templateOptions.label;
      delete field.templateOptions;
    });

    return fields;
  }

};
