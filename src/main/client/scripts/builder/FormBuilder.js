export default class FormBuilder {
  constructor(fields=[]) {
    this.fields = fields;
  }

  getFieldsCopy() {
    return angular.copy(this.fields);
  }

  move(direction, index) {
    const temp = this.fields[index];

    if (direction === 'up' && index > 0) {
      this.fields[index] = this.fields[index - 1];
      this.fields[index - 1] = temp;
    } else if (direction === 'down' && index < this.fields.length-1) {
      this.fields[index] = this.fields[index + 1];
      this.fields[index + 1] = temp;
    }
  }

  remove(field, soft) {
    const resp = window.confirm('Are you sure? All responses for this field will be lost.');

    if (!resp) return;

    if (soft) field.hidden = true;
    else _.remove(this.fields, field);
  }

  add(type) {
    this.fields.push({
      id: this._generateFieldId(),
      type,
      templateOptions: {
        label: 'New input'
      }
    })
  }

  /////////////////////////////////////////////////////////

  // I didnt write this - it was copied from SO but i lost the link :D
  // oh and what it does? generates a GUID thing coz we need a unique id
  _generateFieldId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }
};
