module.exports = [
  'formlyConfig',
  function (formlyConfig) {
    formlyConfig.setType({
      name: 'fileupload',
      link: function ($scope) {
        $scope.onSuccessUpload = response => {
          $scope.model[$scope.options.key] = response.data;
        };
      },
      template: `
        <div
          class="btn btn-primary btn-upload"
          upload-button
          param="uploadFile"
          url="/storage/upload"
          on-success="onSuccessUpload(response)"
        >Upload</div>
      `,
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });
  }
];