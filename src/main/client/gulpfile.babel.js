import gulp from 'gulp';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import sass from 'gulp-sass';
import uglify from 'gulp-uglify';
import buffer from 'vinyl-buffer';
import gulpif from 'gulp-if';
import concat from 'gulp-concat';
import cleanCSS from 'gulp-clean-css';
import del from 'del';
import buildConf from './build_config.js';

let appConfig;

const production = process.env.NODE_ENV === 'production';

gulp.task('scripts', cb => {
  return browserify(appConfig.browserify)
    .transform(babelify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(gulpif(production, uglify()))
    .pipe(gulp.dest(appConfig.dist));
})

gulp.task('stylesheets', () => {
  gulp.src(appConfig.stylesheets)
    .pipe(gulpif(production, cleanCSS({ keepSpecialComments: 0 })))
    .pipe(concat('main.css'))
    .pipe(gulp.dest(appConfig.dist))
});

gulp.task('watch', () => {
	gulp.watch(appConfig.watch.js, ['scripts']);
	gulp.watch(appConfig.watch.css, ['stylesheets']);
});

gulp.task('build', ['stylesheets', 'scripts']);

gulp.task('default', ['build:admin'], () => gulp.start('build:user'));

gulp.task('build:admin', () => {
  appConfig = buildConf.admin;
  gulp.start('build');
});

gulp.task('build:user', () => {
  appConfig = buildConf.user;
  gulp.start('build');
});

gulp.task('clean:admin', cb => del([buildConf.admin.dist], { force: true }));
gulp.task('clean:user', cb => del([buildConf.user.dist], { force: true }));

gulp.task('clean', ['clean:user', 'clean:admin']);

gulp.task('dev:user', ['build:user'], () => gulp.start('watch'));
gulp.task('dev:admin', ['build:admin'], () => gulp.start('watch'));