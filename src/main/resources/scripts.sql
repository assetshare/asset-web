drop database assetshare;
create database if not exists assetshare;
use assetshare;

CREATE TABLE IF NOT EXISTS role (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	role_name VARCHAR(255) NOT NULL,
	role_label VARCHAR(255) NOT NULL,
	role_custom_label VARCHAR(255),
	created TIMESTAMP,
	UNIQUE KEY (role_name),
	UNIQUE KEY (role_label),
	UNIQUE KEY (role_custom_label)
	);

CREATE TABLE IF NOT EXISTS account (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(255) NOT NULL,
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	password VARCHAR(12) NOT NULL,
	created TIMESTAMP,
	role_id INT,
	active TINYINT(1) DEFAULT 0,
	app_owner TINYINT(1) DEFAULT 0,
	UNIQUE KEY (email),
	CONSTRAINT FOREIGN KEY (role_id) references role (id) ON UPDATE restrict
	);


CREATE TABLE IF NOT EXISTS app (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	active TINYINT(1) DEFAULT 1,
	created TIMESTAMP,
	UNIQUE KEY (name) 
	);

CREATE TABLE IF NOT EXISTS app_accounts(
	app_id INT NOT NULL,
	account_id INT NOT NULL,
	CONSTRAINT FOREIGN KEY (account_id) references account (id),
	CONSTRAINT FOREIGN KEY (app_id) references app (id)
	);

	
CREATE TABLE IF NOT EXISTS form (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	subtitle VARCHAR(255) NOT NULL,
	config BLOB NOT NULL,
	active TINYINT(1) DEFAULT 1,
	type VARCHAR(63) NOT NULL,
	app_id INT,
	UNIQUE KEY (app_id,name),
	created TIMESTAMP,
	CONSTRAINT FOREIGN KEY (app_id) references app (id) ON UPDATE restrict 
	);

	CREATE TABLE form_response_seq_store(
 	seq_name VARCHAR(255) NOT NULL,
 	seq_value BIGINT NOT NULL,
	PRIMARY KEY(seq_name)
	);
 
	INSERT INTO form_response_seq_store VALUES ('FORM_RESPONSE_PK', 0);
	
	CREATE TABLE IF NOT EXISTS stored_file (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	filename VARCHAR(255) NOT NULL,
	mime_type VARCHAR(255) NOT NULL,
	hash VARCHAR(255) NOT NULL,
	account_id INT NOT NULL,
	created TIMESTAMP,
	UNIQUE KEY (id) 
	);
	
	
	CREATE TABLE IF NOT EXISTS asset_registration_form_response (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	account_id INT NOT NULL,
	form_id INT NOT NULL,
	config BLOB NOT NULL,
	created TIMESTAMP,
	status varchar(30),
	auto_approve TINYINT(1) DEFAULT 0,
	asset_booking_type VARCHAR(10),
	hourly_rate INT, 
	daily_rate INT,
	available_from_date TIMESTAMP NOT NULL,
	available_to_date TIMESTAMP NOT NULL,
	open_from_hour TIME,
	open_to_hour TIME,
	thumbnail INT,
	CONSTRAINT FOREIGN KEY (account_id) references account (id),
	CONSTRAINT FOREIGN KEY (form_id) references form (id),
	CONSTRAINT FOREIGN KEY (thumbnail) references stored_file (id)
	);

	CREATE TABLE IF NOT EXISTS asset_booking_form_response (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	account_id INT NOT NULL,
	form_id INT NOT NULL,
	config BLOB NOT NULL,
	created TIMESTAMP,
	status varchar(30) DEFAULT 'pending',
	message varchar(255),
	booking_from_date TIMESTAMP NOT NULL,
	booking_to_date TIMESTAMP NOT NULL,
	asset_id INT NOT NULL,
	actioned_by INT,
	actioned_on TIMESTAMP,
	CONSTRAINT FOREIGN KEY (account_id) references account (id),
	CONSTRAINT FOREIGN KEY (form_id) references form (id),
	CONSTRAINT FOREIGN KEY (asset_id) references asset_registration_form_response (id)
);

	CREATE TABLE IF NOT EXISTS stored_file (
	 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	 filename VARCHAR(255) NOT NULL,
	 mime_type VARCHAR(255) NOT NULL,
	 hash VARCHAR(255) NOT NULL,
	 account_id INT NOT NULL,
	 created TIMESTAMP,
	 UNIQUE KEY (id) 
    );
	
INSERT into ROLE(role_name,role_label,role_custom_label) values('SYSTEM_ADMIN','System Admin','System Admin');
INSERT into ROLE(role_name,role_label,role_custom_label) values('PROVIDER','Provider','Provider');
INSERT into ROLE(role_name,role_label,role_custom_label) values('CONSUMER','Consumer','Consumer Custom');

INSERT into account (email,firstname,lastname,password,role_id,app_owner) VALUES ('assetshare101@gmail.com','Sam','Admin','test',1,1);
INSERT into app(name) VALUES("client");

INSERT into app_accounts values (1,1); 

INSERT into account (email,firstname,lastname,password,role_id) VALUES ('test1@gmail.com','John','Smith','test',2);
INSERT into app_accounts(account_id,app_id) values (last_insert_id(),1); 
INSERT into account (email,firstname,lastname,password,role_id) VALUES ('test2@gmail.com','Martin','Stevens','test',2);
INSERT into app_accounts(account_id,app_id) values (last_insert_id(),1); 
INSERT into account (email,firstname,lastname,password,role_id) VALUES ('test3@gmail.com','Joe','Plumber','test',3);
INSERT into app_accounts(account_id,app_id) values (last_insert_id(),1); 
INSERT into account (email,firstname,lastname,password,role_id) VALUES ('test4@gmail.com','Fart','Bloggs','test',3);
INSERT into app_accounts(account_id,app_id) values (last_insert_id(),1); 

INSERT INTO `form` VALUES (1,'Asset Booking Form','Book your asset and enjoy it...','[{\"id\":\"32834488-9e99-4178-a49c-37304ab1f0d6\",\"type\":\"input\",\"templateOptions\":{\"label\":\"First Name\",\"placeholder\":\"Enter your First Name\"}},{\"id\":\"42ea3e9f-f33b-4201-8781-e90fb0575aaa\",\"type\":\"input\",\"templateOptions\":{\"label\":\"Last Name\",\"placeholder\":\"Enter your Last Name\"}}]',1,'BOOKING',1,'2017-01-20 10:08:53'),(2,'Asset Registration Form','Register your asset','[{\"id\":\"9f96a2cb-aeb2-4b1c-aaf3-72205ea03a2f\",\"type\":\"input\",\"templateOptions\":{\"label\":\"Asset Name\"}},{\"id\":\"465eb1cf-7d0a-474d-b6a1-aea182ea1d4e\",\"type\":\"textarea\",\"templateOptions\":{\"label\":\"Asset Description\"}}]',1,'REGISTER',1,'2017-01-20 10:09:40');





