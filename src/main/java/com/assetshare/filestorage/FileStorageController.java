package com.assetshare.filestorage;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.assetshare.entities.Account;
import com.assetshare.entities.StoredFile;
import com.assetshare.repository.AccountRepository;
import com.assetshare.repository.StoredFileRepository;

@Controller
@RequestMapping("/storage")
public class FileStorageController {
	
	@Autowired
	private StoredFileRepository fileRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private ServletContext servletContext;
	
	private final static String STORAGE_BASE_PATH = "/storage/";
	
	@GetMapping(value="/{fileId}")
	public void downloadFile(@PathVariable Long fileId,HttpServletResponse response) throws IOException {
		StoredFile storedFile = fileRepository.findOne(fileId);
		final String hash = storedFile.getHash();
		final String storagePath = STORAGE_BASE_PATH + hash;
		final File file = new File(servletContext.getRealPath(storagePath));
		InputStream myStream = new FileInputStream(file);

		// Set the content type and attachment header.
		response.addHeader("Content-disposition", "attachment;filename=" + storedFile.getFilename());
		response.setContentType(storedFile.getMimeType());

		IOUtils.copy(myStream, response.getOutputStream());
		response.flushBuffer();
	}

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public StoredFile uploadFile(@RequestParam("uploadFile") MultipartFile uploadFile,@ModelAttribute("loggedInAccount") Long accountId) {
      try {
    	  Account uploader = accountRepository.findOne(accountId);
    	  StoredFile storedFile = storeFile(uploadFile);
    	  storedFile.setUploader(uploader);
    	  fileRepository.save(storedFile);
  		  storedFile.setFileUrl(STORAGE_BASE_PATH + storedFile.getId());
    	  return storedFile;
      }
      catch (Exception e) {
    	return null;
      }
    }
    
    public StoredFile storeFile(MultipartFile uploadFile) throws IOException, NoSuchAlgorithmException {
		byte[] inputBytes = uploadFile.getBytes();
    	if (inputBytes == null || inputBytes.length <= 0) {
			return null;
		}
		String filename = uploadFile.getOriginalFilename();
        String mimeType = uploadFile.getContentType();
		// otherwise get hash
		final String hash = DigestUtils.md5Hex(inputBytes);
		final String storagePath = STORAGE_BASE_PATH + hash;
		
		File storageDir = new File(servletContext.getRealPath(STORAGE_BASE_PATH));
		
		if(!storageDir.exists()){
			storageDir.mkdirs();
		}
		
		// get real file path
		final File file = new File(servletContext.getRealPath(storagePath));
		if (!file.exists()) {
			BufferedOutputStream stream =
	            new BufferedOutputStream(new FileOutputStream(file));
			stream.write(inputBytes);
			stream.close();
		}
		return new StoredFile(filename, mimeType, hash); 
	}

}
