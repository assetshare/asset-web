package com.assetshare.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.assetshare.gravator.Gravatar;
import com.assetshare.gravator.GravatarDefaultImage;
import com.assetshare.gravator.GravatarRating;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column
	@NotNull
	private String password;

	@Column
	@NotNull
	private String firstname;

	@Column
	@NotNull
	private String lastname;

	@Column(unique = true)
	@NotNull
	private String email;

	@Column(insertable = false, updatable = false)
	private Date created;

	@JoinColumn(name = "role_id")
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.JOIN)
	private RoleMaster role;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinTable(name = "app_accounts", joinColumns = { @JoinColumn(name = "account_id") }, inverseJoinColumns = {
			@JoinColumn(name = "app_id") })
	private App app;

	@Column
	private boolean active;

	@Column(name = "app_owner")
	private boolean appOwner;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public RoleMaster getRole() {
		return role;
	}

	public void setRole(RoleMaster role) {
		this.role = role;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isAppOwner() {
		return appOwner;
	}

	public void setAppOwner(boolean appOwner) {
		this.appOwner = appOwner;
	}
	
	public String getGravator(){
		Gravatar gravatar = new Gravatar();
		gravatar.setRating(GravatarRating.GENERAL_AUDIENCES);
		return gravatar.getUrl(email);
	}
}
