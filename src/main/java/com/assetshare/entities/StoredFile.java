package com.assetshare.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "stored_file")
public class StoredFile {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column
	@NotNull
	private String filename;
	
	@Column(name ="mime_type")
	@NotNull
	private String mimeType;
	
	@Column
	@NotNull
	@JsonIgnore
	private String hash;
	
	@JoinColumn(name = "account_id", nullable=false)
	@OneToOne(fetch=FetchType.EAGER)
	@JsonIgnore
	private Account uploader;
	
	@Transient
	@NotNull
	private String ext="";
	
	@Transient
	private String fileUrl;
	
	public StoredFile(){}
	
	public StoredFile(String filename, String mimeType, String hash) {
		this.filename = filename;
		this.mimeType = mimeType;
		this.hash = hash;
		if(filename != null && filename.indexOf(".") > 0)
			this.ext = filename.split("\\.")[1];
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getMimeType() {
		return mimeType;
	}
	
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public Account getUploader() {
		return uploader;
	}

	public void setUploader(Account uploader) {
		this.uploader = uploader;
	}

	public String getExt() {
		return ext;
	}
	
	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	
}
