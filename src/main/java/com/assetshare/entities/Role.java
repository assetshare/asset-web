package com.assetshare.entities;

public enum Role {
	SYSTEM_ADMIN("SYSTEM_ADMIN"), PROVIDER("PROVIDER"), CONSUMER("CONSUMER");

	String name;

	private Role(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
