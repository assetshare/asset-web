package com.assetshare.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.util.StringUtils;

@Entity
@Table(name = "role")
public class RoleMaster {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "role_name", unique = true)
	@NotNull
	private String roleName;

	@Column(name = "role_label", unique = true)
	@NotNull
	private String label;

	@Column(name = "role_custom_label", unique = true)
	private String customLabel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Role getRoleName() {
		return Role.valueOf(roleName);
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getLabel() {
		return StringUtils.isEmpty(customLabel) ? label : customLabel;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCustomLabel() {
		return customLabel;
	}

	public void setCustomLabel(String customLabel) {
		this.customLabel = customLabel;
	}
}