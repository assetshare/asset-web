package com.assetshare.entities;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.assetshare.form.entities.Form;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "app")
public class App {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	@NotNull
	private String name;

	@Column(columnDefinition = "TINYINT(1)")
	private boolean active = true;

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	private Set<Form> forms;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "app_accounts", joinColumns = @JoinColumn(name = "app_id"), inverseJoinColumns = @JoinColumn(name = "account_id", referencedColumnName = "id"))
	@JsonIgnore
	private List<Account> accounts;

	public Long getId() {
		return id;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<Form> getForms() {
		return forms;
	}

	public void setForms(Set<Form> forms) {
		this.forms = forms;
	}
}
