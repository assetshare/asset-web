package com.assetshare.form;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.assetshare.AbstractController;
import com.assetshare.configuration.EnumTypeEditor;
import com.assetshare.entities.App;
import com.assetshare.form.entities.Form;
import com.assetshare.form.repository.FormRepository;
import com.assetshare.repository.AppRepository;

@Controller
public class FormController extends AbstractController{

	@Autowired
	private FormRepository formRepository;

	@Autowired
	private AppRepository appRepository;
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.registerCustomEditor(Form.Type.class, new EnumTypeEditor<>(Form.Type.class));
	}

	@GetMapping(value={"/{appName}/forms/{formType}/load","/forms/{formType}/load"})
	public @ResponseBody Form getBookingForm(@PathVariable Form.Type formType, @PathVariable Optional<String> appName,HttpServletRequest request) {
		App app = getApp(request);
		if(app == null && !StringUtils.isEmpty(appName)){
			app = appRepository.findByName(appName.get());
		}
		return formRepository.findByAppAndType(app, formType.toString());
	}

	@PostMapping(value = "/{appName}/formbuilder", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Form buildForm(@RequestBody Form form, @PathVariable String appName,HttpServletResponse response) {
		try {
			form.setApp(appRepository.findByName(appName));
			form.populateConfig();
			return formRepository.save(form);
		} catch (Exception exception) {
			response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
			return null;
		}
	}
}
