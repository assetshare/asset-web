package com.assetshare.form.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.DiscriminatorOptions;
import org.springframework.util.StringUtils;

import com.assetshare.entities.Account;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.databind.JsonNode;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorOptions(force=true)

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=As.WRAPPER_OBJECT, property="type")
@JsonSubTypes({
      @JsonSubTypes.Type(value=AssetBookingFormResponse.class, name="booking"),
      @JsonSubTypes.Type(value=AssetRegistrationFormResponse.class, name="register")
  }) 
public abstract class FormResponse {
	
	@Id
	@TableGenerator(name="seqStore", table="form_response_seq_store",pkColumnName="seq_name",pkColumnValue="FORM_RESPONSE_PK",valueColumnName="seq_value",initialValue=1, allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE,generator="seqStore")
	private Long id;
	
	@JoinColumn(name = "account_id", nullable=false)
	@OneToOne(fetch=FetchType.EAGER)
	@JsonIgnore
	private Account submitter;
	
	@JoinColumn(name = "form_id", nullable=false)
	@OneToOne(fetch=FetchType.EAGER)
	@JsonIgnore
	private Form form;
	
	@Transient
	private JsonNode config;
	
	@Column(name="config")
	@JsonIgnore
	private String configRaw;
	
	@Column
	private String status = "pending";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Account getSubmitter() {
		return submitter;
	}

	public void setSubmitter(Account submitter) {
		this.submitter = submitter;
	}

	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	@JsonRawValue
	public String getConfig() {
		return !StringUtils.isEmpty(configRaw) ? configRaw.toString() : "{}";
	}

	public void setConfig(JsonNode config) {
		this.config = config;
	}
    
	public String getConfigRaw() {
		return configRaw;
	}

	public void setConfigRaw(String configRaw) {
		this.configRaw = configRaw;
	}
	
	@PrePersist
	public void populateConfig(){
		this.configRaw = config != null ? config.toString() : "";
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getType(){
		return "unknown";
	}
}
