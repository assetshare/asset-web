package com.assetshare.form.entities;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.assetshare.entities.StoredFile;
import com.assetshare.form.entities.Form.Type;

@Entity
@Table(name = "asset_registration_form_response")
@DiscriminatorValue("register")
public class AssetRegistrationFormResponse extends FormResponse{

	@Column(name="auto_approve",columnDefinition = "TINYINT(1)")
	private boolean autoApprove = false;
	
	@Column(name="available_from_date")
	private Date availableFrom;
	
	@Column(name="available_to_date")
	private Date availableTo;
	
	@Column(name="open_from_hour")
	private Time openFrom;
	
	@Column(name="open_to_hour")
	private Time openTo;
	
	@Column(name="hourly_rate")
	private int hourlyRate;
	
	@Column(name="daily_rate")
	private int dailyRate;

	@Column(name="asset_booking_type")
	private String bookingType;
	
	@Column
	private String title;
	
	@JoinColumn(name = "thumbnail")
	@ManyToOne(cascade = CascadeType.ALL)
	private StoredFile thumbnail;
	

	public boolean isAutoApprove() {
		return autoApprove;
	}

	public void setAutoApprove(boolean autoApprove) {
		this.autoApprove = autoApprove;
	}

	public Date getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(Date availableFrom) {
		this.availableFrom = availableFrom;
	}

	public Date getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(Date availableTo) {
		this.availableTo = availableTo;
	}

	public Time getOpenFrom() {
		return openFrom;
	}

	public void setOpenFrom(Time openFrom) {
		this.openFrom = openFrom;
	}

	public Time getOpenTo() {
		return openTo;
	}

	public void setOpenTo(Time openTo) {
		this.openTo = openTo;
	}

	public int getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(int hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public int getDailyRate() {
		return dailyRate;
	}

	public void setDailyRate(int dailyRate) {
		this.dailyRate = dailyRate;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public StoredFile getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(StoredFile thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public String getThumbnailUrl() {
		if (this.thumbnail != null) {
			return "/storage/" + this.thumbnail.getId();
		}

		return "/images/default-thumbnail.png";
	}

	@Override
	public String getType(){
		return Type.REGISTER.name();
	}
}
