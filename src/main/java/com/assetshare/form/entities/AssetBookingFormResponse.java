package com.assetshare.form.entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.assetshare.entities.Account;
import com.assetshare.form.entities.Form.Type;

@Entity
@Table(name = "asset_booking_form_response")
@DiscriminatorValue("booking")
public class AssetBookingFormResponse extends FormResponse{

	@Column
	private String message;
	
	@Column(name="booking_from_date")
	private Date bookingFrom;
	
	@Column(name="booking_to_date")
	private Date bookingTo;
	
	@JoinColumn(name = "asset_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private AssetRegistrationFormResponse asset;
	
	@JoinColumn(name = "actioned_by")
	@OneToOne(fetch=FetchType.EAGER)
	private Account actionedBy;
	
	@Column(name="actioned_on")
	private Date actionedOn;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getBookingFrom() {
		return bookingFrom;
	}

	public void setBookingFrom(Date bookingFrom) {
		this.bookingFrom = bookingFrom;
	}

	public Date getBookingTo() {
		return bookingTo;
	}

	public void setBookingTo(Date bookingTo) {
		this.bookingTo = bookingTo;
	}

	public AssetRegistrationFormResponse getAsset() {
		return asset;
	}

	public void setAsset(AssetRegistrationFormResponse asset) {
		this.asset = asset;
	}

	public Date getActionedOn() {
		return actionedOn;
	}

	public void setActionedOn(Date actionedOn) {
		this.actionedOn = actionedOn;
	}

	public Account getActionedBy() {
		return actionedBy;
	}

	public void setActionedBy(Account actionedBy) {
		this.actionedBy = actionedBy;
	}

	@Override
	public String getType(){
		return Type.BOOKING.name();
	}
}
