package com.assetshare.form.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.util.StringUtils;

import com.assetshare.entities.App;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JsonNode;

@Entity
@Table(name = "form")
public class Form {

	public enum Type {
		BOOKING("booking"), REGISTER("register");

		String name;

		private Type(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column
	@NotNull
	private String name;

	@Column
	@NotNull
	private String subtitle;

	@Transient
	private JsonNode config;

	@Column(name = "config")
	@JsonIgnore
	private String configRaw;

	@Column
	@NotNull
	private String type;

	@Column(columnDefinition = "TINYINT(1)")
	private boolean active = true;

	@JoinColumn(name = "app_id")
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonIgnore
	private App app;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	@JsonRawValue
	public String getConfig() {
		return !StringUtils.isEmpty(configRaw) ? configRaw.toString() : "{}";
	}

	public void setConfig(JsonNode config) {
		this.config = config;
	}

	public String getConfigRaw() {
		return configRaw;
	}

	public void setConfigRaw(String configRaw) {
		this.configRaw = configRaw;
	}

	@PrePersist
	public void populateConfig() {
		this.configRaw = config != null ? config.toString() : "";
	}

	public Type getType() {
		return Type.valueOf(type);
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}
}
