package com.assetshare.form.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assetshare.entities.Account;
import com.assetshare.form.entities.AssetBookingFormResponse;
import com.assetshare.form.entities.AssetRegistrationFormResponse;
import com.assetshare.form.entities.Form;
import com.assetshare.form.entities.FormResponse;

@Repository
public interface FormResponseRepository extends CrudRepository<FormResponse, Long> {
	List<FormResponse> findByForm(Form form);
	
	List<AssetRegistrationFormResponse> findByTitleIgnoreCaseContaining(String title);
	
	List<AssetRegistrationFormResponse> findBySubmitter(Account account);
	
	List<AssetBookingFormResponse> findByAsset(FormResponse asset);
	
	List<FormResponse> findByFormAndSubmitter(Form form,Account submitter);
}
