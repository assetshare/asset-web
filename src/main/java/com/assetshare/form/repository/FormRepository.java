package com.assetshare.form.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assetshare.entities.App;
import com.assetshare.form.entities.Form;

@Repository
public interface FormRepository extends CrudRepository<Form, Long> {
	Form findByAppAndType(App app, String type);
}
