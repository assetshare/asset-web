package com.assetshare.dashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.entities.Account;
import com.assetshare.entities.Role;
import com.assetshare.entities.RoleMaster;
import com.assetshare.repository.AccountRepository;
import com.assetshare.repository.RoleRepository;

@Controller
public class DashboardController {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("{appName}/manage/dashboard")
	public ModelAndView redirectToAdminDashboard(@ModelAttribute("loggedInAccount") Long accountId) {
		Account account = accountRepository.findOne(accountId);
		List<Account> dashboardAccountsData = new ArrayList<Account>();
		List<RoleMaster> roles = roleRepository
				.findByRoleNameIn(Arrays.asList(Role.CONSUMER.toString(), Role.PROVIDER.toString()));
		if (roles != null && !roles.isEmpty()) {
			dashboardAccountsData = accountRepository.findTop10ByRoleInOrderByCreatedDesc(roles);
		}
		ModelAndView dashboardModel = new ModelAndView("manage/dashboard/index");
		dashboardModel.addObject("account", account);
		dashboardModel.addObject("dashboardAccountsData", dashboardAccountsData);
		return dashboardModel;
	}

	@GetMapping("client/dashboard")
	public ModelAndView redirectToClientDashboard(@ModelAttribute("loggedInAccount") Long accountId) {
		Account account = accountRepository.findOne(accountId);
		ModelAndView dashboardModel = new ModelAndView("portal/dashboard/index");
		dashboardModel.addObject("account", account);
		return dashboardModel;
	}
}
