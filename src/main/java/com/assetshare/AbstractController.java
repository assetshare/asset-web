package com.assetshare;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.assetshare.entities.App;
import com.assetshare.repository.AppRepository;

public class AbstractController {

	@Autowired
	private AppRepository appRepository;
	
	protected App getApp(HttpServletRequest request){
		StringBuffer requestURL = request.getRequestURL();
		if(!requestURL.toString().startsWith("assetshare") && !requestURL.toString().startsWith("http://assetshare")){
			String appName = requestURL.substring(0, requestURL.toString().indexOf("."));
			if(!StringUtils.isEmpty(appName)){
				appName = appName.replaceAll("http://","");
				return appRepository.findByName(appName);
			}
		}
		return null;
	}
}
