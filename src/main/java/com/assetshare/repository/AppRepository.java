package com.assetshare.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assetshare.entities.App;

@Repository
public interface AppRepository extends CrudRepository<App, Long> {
	App findById(Long id);

	App findByName(String appName);

}
