package com.assetshare.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assetshare.entities.RoleMaster;

@Repository
public interface RoleRepository extends CrudRepository<RoleMaster, Long> {

	RoleMaster findByRoleName(String roleName);

	List<RoleMaster> findByRoleNameIn(List<String> roleNames);
}