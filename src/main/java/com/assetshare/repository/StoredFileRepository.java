package com.assetshare.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assetshare.entities.StoredFile;

@Repository
public interface StoredFileRepository extends JpaRepository<StoredFile, Long>{

}
