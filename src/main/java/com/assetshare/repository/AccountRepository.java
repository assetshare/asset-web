package com.assetshare.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assetshare.entities.Account;
import com.assetshare.entities.RoleMaster;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByEmailAndPasswordIgnoringCase(String email, String password);

	List<Account> findByRole(RoleMaster role);

	List<Account> findDistinctByRoleIn(List<RoleMaster> roles);

	List<Account> findTop10ByRoleInOrderByCreatedDesc(List<RoleMaster> roles);
}
