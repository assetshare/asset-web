package com.assetshare.signup;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.AbstractController;
import com.assetshare.entities.Account;
import com.assetshare.entities.Role;
import com.assetshare.repository.AccountRepository;
import com.assetshare.repository.RoleRepository;

@Controller
public class SignupController extends AbstractController{

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping(value={"/signup","/portal/register"})
	public ModelAndView redirectToSignupPage(@RequestParam(name="as",required=false) String as) {
		ModelAndView loginModelView = new ModelAndView(StringUtils.isEmpty(as)?"/manage/signup":"/portal/register");
		loginModelView.addObject("user", new Account());
		return loginModelView;
	}
	
	@PostMapping(value={"/signup","/register"})
	public String createAccount(@ModelAttribute Account account, HttpServletRequest request,@RequestParam(name="as",required=false) String as) {
		System.out.println("Registering");
		account.setActive(true);
		if(StringUtils.isEmpty(as)){
			account.setRole(roleRepository.findByRoleName(Role.SYSTEM_ADMIN.name()));
			account.setAppOwner(true);
		}else{
			account.setRole(roleRepository.findByRoleName(Role.CONSUMER.name()));
			account.setApp(getApp(request));
		}
		accountRepository.save(account);

		request.getSession().setAttribute("loggedInAccount", account.getId());
		request.getSession().setAttribute("appName", account.getApp().getName());
		if(StringUtils.isEmpty(as)){
			System.out.println("redirecting");
			return "redirect:" + "/manage/";
		}
		return "redirect:" + "/client/dashboard";
	}
}
