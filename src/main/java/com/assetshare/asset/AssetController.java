package com.assetshare.asset;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.AbstractController;
import com.assetshare.entities.Account;
import com.assetshare.form.entities.AssetBookingFormResponse;
import com.assetshare.form.entities.AssetRegistrationFormResponse;
import com.assetshare.form.entities.Form;
import com.assetshare.form.entities.FormResponse;
import com.assetshare.form.repository.FormRepository;
import com.assetshare.form.repository.FormResponseRepository;
import com.assetshare.repository.AccountRepository;
import com.assetshare.repository.AppRepository;
import com.assetshare.repository.StoredFileRepository;

@Controller
public class AssetController extends AbstractController{

	@Autowired
	private FormResponseRepository formResponseRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private FormRepository formRepository;
	
	@Autowired
	private AppRepository appRepository;
	
	@Autowired
	private StoredFileRepository fileRepository;
	
	@GetMapping("/{appName}/assets")
	public ModelAndView getAssets(@ModelAttribute("loggedInAccount") Long accountId) {
		Account account = accountRepository.findOne(accountId);
		ModelAndView formsModelAndView = new ModelAndView("manage/asset/index");
		Form bookingForm = formRepository.findByAppAndType(account.getApp(), Form.Type.REGISTER.toString());
		List<FormResponse> assets = formResponseRepository.findByForm(bookingForm);
		formsModelAndView.addObject("assets",assets);
		return formsModelAndView;
	}
	
	@GetMapping("/{appName}/assets/{id}")
	public ModelAndView getAssetById(@ModelAttribute("loggedInAccount") Long accountId,@PathVariable Long id) {
		return getAsset(id,"manage/asset/edit");
	}
	
	@GetMapping("/{appName}/assets/{id}/delete")
	public ModelAndView markAssetDelete(@ModelAttribute("loggedInAccount") Long accountId,@PathVariable Long id,@PathVariable String appName) {
		try{
			formResponseRepository.delete(id);
			return getAsset(id,appName+"/assets");
		}catch(Exception exception){
			ModelAndView formsModelAndView = getAssets(accountId);
			formsModelAndView.addObject("errorMessage", "Unable to delete Asset due to " + exception.getMessage());
			return formsModelAndView;
		}
	}

	private ModelAndView getAsset(Long id,String viewName) {
		ModelAndView formsModelAndView = new ModelAndView(viewName);
		FormResponse asset = formResponseRepository.findOne(id);
		List<AssetBookingFormResponse> bookings=  formResponseRepository.findByAsset(asset);
		formsModelAndView.addObject("asset",asset);
		formsModelAndView.addObject("bookings",bookings);
		return formsModelAndView;
	}
	
	@GetMapping("/listings/{id}")
	public ModelAndView getAssetListingById(@ModelAttribute("loggedInAccount") Long accountId,@PathVariable Long id) {
		return getAsset(id,"portal/asset/edit");
	}
	
	
	@GetMapping("/assets/register")
	public ModelAndView getAssetRegisterPage(HttpServletRequest request){
		ModelAndView formsModelAndView = new ModelAndView("portal/asset/register");
		Form form =  formRepository.findByAppAndType(appRepository.findByName(getApp(request).getName()), Form.Type.REGISTER.toString());
		formsModelAndView.addObject("assetRegister", form);
		return formsModelAndView;
	}
	
	@GetMapping("/listings")
	public ModelAndView getAssetsForAccount(@ModelAttribute("loggedInAccount") Long accountId){
		Account account = accountRepository.findOne(accountId);
		ModelAndView formsModelAndView = new ModelAndView("portal/asset/myasset");
		List<AssetRegistrationFormResponse> listings = formResponseRepository.findBySubmitter(account);
		formsModelAndView.addObject("listings",listings);
		return formsModelAndView;
	}
	
	@PostMapping("/assets/register")
	public @ResponseBody FormResponse saveAssetRegistration(@RequestBody FormResponse formResponse,@ModelAttribute("loggedInAccount") Long accountId,HttpServletRequest request){
		try {
			Account account = accountRepository.findOne(accountId);
			Form form = formRepository.findByAppAndType(getApp(request), formResponse.getType());
			formResponse.setForm(form);
			formResponse.setSubmitter(account);
			formResponse.populateConfig();
			
			if(formResponse instanceof AssetRegistrationFormResponse){
				AssetRegistrationFormResponse assetRegistration = (AssetRegistrationFormResponse)formResponse;
				if(assetRegistration.getThumbnail() != null && assetRegistration.getThumbnail().getId() > 0){
					assetRegistration.setThumbnail(fileRepository.findOne(assetRegistration.getThumbnail().getId()));
				}else{
					assetRegistration.setThumbnail(null);
				}
			}
			request.getSession().setAttribute("message", "Asset successfully created");
			return formResponseRepository.save(formResponse);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
}
