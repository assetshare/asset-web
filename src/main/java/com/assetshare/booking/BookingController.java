package com.assetshare.booking;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.AbstractController;
import com.assetshare.entities.Account;
import com.assetshare.form.entities.AssetBookingFormResponse;
import com.assetshare.form.entities.AssetRegistrationFormResponse;
import com.assetshare.form.entities.Form;
import com.assetshare.form.entities.FormResponse;
import com.assetshare.form.repository.FormRepository;
import com.assetshare.form.repository.FormResponseRepository;
import com.assetshare.repository.AccountRepository;
import com.assetshare.repository.AppRepository;

@Controller
public class BookingController extends AbstractController{

	@Autowired
	private FormResponseRepository formResponseRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private FormRepository formRepository;
	
	@Autowired
	private AppRepository appRepository;
	
	@GetMapping("/{accountId}/bookings")
	public ModelAndView getAssets(@PathVariable Long accountId) {
		Account account = accountRepository.findOne(accountId);
		ModelAndView formsModelAndView = new ModelAndView("/portal/booking/mybookings");
		Form bookingForm = formRepository.findByAppAndType(account.getApp(), Form.Type.BOOKING.toString());
		List<FormResponse> bookings = formResponseRepository.findByFormAndSubmitter(bookingForm,account);
		formsModelAndView.addObject("bookings",bookings);
		return formsModelAndView;
	}
	
	@GetMapping("/assets/{id}/book")
	public ModelAndView showBookingForm(@PathVariable Long id,HttpServletRequest request) {
		ModelAndView formsModelAndView = new ModelAndView("/portal/booking/index");
		Form form = formRepository.findByAppAndType(appRepository.findByName(getApp(request).getName()), Form.Type.BOOKING.toString());
		FormResponse asset = formResponseRepository.findOne(id);
		formsModelAndView.addObject("bookingForm", form);
		formsModelAndView.addObject("asset", asset);
		return formsModelAndView;
	}
	
	@GetMapping("/booking/{id}/{decision}")
	public String finaliseBooking(@ModelAttribute("loggedInAccount") Long accountId,@PathVariable Long id,@PathVariable String decision) {
		Account account = accountRepository.findOne(accountId);
		AssetBookingFormResponse booking = (AssetBookingFormResponse)formResponseRepository.findOne(id);
		booking.setActionedBy(account);
		booking.setStatus(decision);
		formResponseRepository.save(booking);
		return "redirect:/listings/"+booking.getAsset().getId();
	}
	
	@PostMapping("/assets/{id}/book")
	public @ResponseBody FormResponse bookAsset(@PathVariable Long id,@RequestBody FormResponse formResponse,@ModelAttribute("loggedInAccount") Long accountId,HttpServletRequest request) {
		Account account = accountRepository.findOne(accountId);
		Form form = formRepository.findByAppAndType(appRepository.findByName(getApp(request).getName()),formResponse.getType());
		formResponse.setForm(form);
		formResponse.setSubmitter(account);
		formResponse.populateConfig();
		AssetRegistrationFormResponse asset = (AssetRegistrationFormResponse)formResponseRepository.findOne(id);
		if(formResponse instanceof AssetBookingFormResponse){
			AssetBookingFormResponse assetBooking = (AssetBookingFormResponse)formResponse;
			assetBooking.setAsset(asset);
			assetBooking.setStatus("pending");
		}
		return formResponseRepository.save(formResponse);
	}
}
