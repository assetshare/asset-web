package com.assetshare.search;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.assetshare.form.repository.FormResponseRepository;

@Controller
public class SearchController {

	@Autowired
	private FormResponseRepository formResponseRepository;
	
	@GetMapping("search")
	public String doSearch(@RequestParam String searchString,HttpServletRequest request){
		request.setAttribute("searchResults",formResponseRepository.findByTitleIgnoreCaseContaining(searchString));
		return "portal/search/index";
	}
}
