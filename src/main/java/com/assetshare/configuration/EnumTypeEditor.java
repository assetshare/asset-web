package com.assetshare.configuration;

import java.beans.PropertyEditorSupport;

public class EnumTypeEditor<T extends Enum<T>> extends PropertyEditorSupport {
	private final Class<T> typeParameterClass;

	public EnumTypeEditor(Class<T> typeParameterClass) {
		super();
		this.typeParameterClass = typeParameterClass;
	}

	@Override
	public void setAsText(final String text) throws IllegalArgumentException {
		String upper = text.toUpperCase();
		T value = Enum.valueOf(typeParameterClass, upper);
		setValue(value);
	}
}
