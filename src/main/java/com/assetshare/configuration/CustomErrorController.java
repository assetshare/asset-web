package com.assetshare.configuration;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {

	@Override
	@RequestMapping("/error")
	public String getErrorPath() {
		return "error";
	}

}
