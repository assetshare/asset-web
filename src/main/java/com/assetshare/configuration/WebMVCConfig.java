package com.assetshare.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("**/portal/login").setViewName("portal/login");
		registry.addViewController("**/forms/booking").setViewName("manage/form/booking");
		registry.addViewController("**/forms/register").setViewName("manage/form/register");
		registry.addViewController("**/support").setViewName("manage/static/support");

		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}
}
