package com.assetshare.mail.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {

	@Autowired
	private JavaMailSender mailSender;

	public void send(String to,String subject,String message) {
		MimeMessage mail = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(to);
			helper.setFrom("assetshare101@gmail.com");
			helper.setSubject(subject);
			helper.setText(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
		}
		mailSender.send(mail);
	}
}
