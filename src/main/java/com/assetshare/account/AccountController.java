package com.assetshare.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.entities.Account;
import com.assetshare.repository.AccountRepository;

@Controller
@RequestMapping("/{appName}/")
public class AccountController {

	@Autowired
	private AccountRepository accountRepository;

	@GetMapping("account/{accountId}")
	public ModelAndView getAccountDetail(@PathVariable Long accountId) {
		Account account = accountRepository.findOne(accountId);
		ModelAndView editAccountModel = new ModelAndView("/manage/user/edit_account");
		editAccountModel.addObject("account", account);
		return editAccountModel;
	}

	@PostMapping("/account")
	public String updateAccount(@ModelAttribute Account updatedAccount) {
		Account account = accountRepository.findOne(updatedAccount.getId());
		account.setFirstname(updatedAccount.getFirstname());
		account.setLastname(updatedAccount.getLastname());
		if (!StringUtils.isEmpty(updatedAccount.getPassword())) {
			account.setPassword(updatedAccount.getPassword());
		}
		accountRepository.save(account);
		return "redirect:account/" + account.getId();
	}
}
