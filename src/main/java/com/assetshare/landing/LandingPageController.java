package com.assetshare.landing;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.AbstractController;
import com.assetshare.entities.Account;
import com.assetshare.entities.App;
import com.assetshare.repository.AccountRepository;

@Controller
public class LandingPageController extends AbstractController{
	
	@Autowired
	private AccountRepository accountRepository;

	@GetMapping("/")
	public ModelAndView portal(HttpServletRequest request){
		App app = getApp(request);
		ModelAndView modelAndView = new ModelAndView("index");
		if(app != null){
			modelAndView.setViewName("portal/index");
			request.setAttribute("appName", app != null ? app.getName() :"");	
		}
		return modelAndView;
	}
	
	@GetMapping("/manage")
	public ModelAndView manage(HttpServletRequest request,@ModelAttribute("errorMessage") String errorMessage,@ModelAttribute("loggedInAccount") Long accountId){
		ModelAndView modelAndView = new ModelAndView("manage/login");
		App app = getApp(request);
		if(accountId > 0){
			Account account = accountRepository.findOne(accountId);
			String redirectPath = "redirect:/" + account.getApp().getName() + "/";
			modelAndView.setViewName(redirectPath + "manage/dashboard");
		}else{
			request.setAttribute("appName", app != null ? app.getName() :"");
			request.setAttribute("errorMessage", errorMessage);
			request.setAttribute("targetUrl", request.getRequestURI());
		}
		return modelAndView;
	}
}
