package com.assetshare.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.assetshare.entities.Account;
import com.assetshare.entities.App;
import com.assetshare.repository.AccountRepository;
import com.assetshare.repository.AppRepository;
import com.assetshare.repository.RoleRepository;

@Controller
@RequestMapping("/{appName}/")
public class UserController {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private AppRepository appRepository;

	@GetMapping("users")
	public ModelAndView getUsers(@PathVariable String appName) {
		ModelAndView formsModelAndView = new ModelAndView("manage/user/users");
		App app = appRepository.findByName(appName);
		formsModelAndView.addObject("users", app.getAccounts());
		return formsModelAndView;
	}

	@GetMapping("{userId}/edit")
	public ModelAndView getUser(@PathVariable Long userId) {
		ModelAndView formsModelAndView = new ModelAndView("manage/user/editUser");
		formsModelAndView.addObject("user", accountRepository.findOne(userId));
		formsModelAndView.addObject("roles", roleRepository.findAll());
		return formsModelAndView;
	}

	@PostMapping("{userId}/update")
	public ModelAndView updateUser(@ModelAttribute Account user, @PathVariable Long userId) {
		Account account = accountRepository.findOne(userId);
		account.setFirstname(user.getFirstname());
		account.setLastname(user.getLastname());
		account.setRole(user.getRole());
		account.setActive(user.isActive());
		accountRepository.save(account);
		ModelAndView updatedUserModel = getUser(userId);
		updatedUserModel.addObject("successMsg", "User updated successfully");
		return updatedUserModel;
	}

}
