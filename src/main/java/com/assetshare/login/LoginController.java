package com.assetshare.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.assetshare.entities.Account;
import com.assetshare.entities.Role;
import com.assetshare.repository.AccountRepository;

@Controller
public class LoginController {
	@Autowired
	private AccountRepository accountRepository;
	
	@PostMapping("/{appName}/login")
	public String authenticate(@ModelAttribute("user") Account user, HttpServletRequest request,RedirectAttributes redirectAttributes) {
		return doAuthenticate(user, request, false,redirectAttributes);
	}

	@PostMapping("login")
	public String authenticateUser(@ModelAttribute("user") Account user, HttpServletRequest request,RedirectAttributes redirectAttributes) {
		return doAuthenticate(user, request, true,redirectAttributes);
	}
	
	@GetMapping("login")
	public String redirectToLogin(@ModelAttribute("user") Account user, HttpServletRequest request,RedirectAttributes redirectAttributes) {
		return doAuthenticate(user, request, true,redirectAttributes);
	}

	private boolean isValidSubDomainLogin(HttpServletRequest request) {
		return request.getRequestURL().toString().startsWith("http://assetshare") ;
	}

	private String doAuthenticate(Account user, HttpServletRequest request, boolean includeAppName,RedirectAttributes redirectAttributes) {
		Account account = accountRepository.findByEmailAndPasswordIgnoringCase(user.getEmail(), user.getPassword());
		String targetUrl = request.getParameter("targetUrl");
		
		if (account != null && account.getId() > 0) {
			if (!isValidSubDomainLogin(request) && account.getRole().getRoleName().equals(Role.SYSTEM_ADMIN)) {
				redirectAttributes.addFlashAttribute("errorMessage", "Invalid Login, you don't own this app");
			} else {
				HttpSession session = request.getSession();
				session.setAttribute("loggedInAccount", account.getId());

				switch (account.getRole().getRoleName()) {
				case SYSTEM_ADMIN:
					session.setAttribute("appName", account.getApp().getName());
					if(StringUtils.isEmpty(targetUrl)){
						String redirectPath = "redirect:" + (includeAppName ? account.getApp().getName() + "/" : "");
						return redirectPath + "manage/dashboard";
					}
					return "redirect:"+targetUrl;
				case CONSUMER: 
				case PROVIDER:
					session.setAttribute("appId", account.getApp().getId());
					return StringUtils.isEmpty(targetUrl)?"redirect:client/dashboard":"redirect:"+targetUrl;
				default:
					break;
				}
			}
		}
		redirectAttributes.addFlashAttribute("targetUrl",targetUrl);
		return "redirect:manage";
	}

	@GetMapping(value={"/{appName}/logout","/logout"})
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}

}
