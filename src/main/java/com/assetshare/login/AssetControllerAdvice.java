package com.assetshare.login;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class AssetControllerAdvice {

	@ModelAttribute("loggedInAccount")
	public Long setAuthenticatedAccount(HttpServletRequest request) {
		if (request.getSession().getAttribute("loggedInAccount") != null) {
			return (Long) request.getSession().getAttribute("loggedInAccount");
		}
		return 0L;
	}
}
