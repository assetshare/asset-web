<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section>
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->
    <h3>
      Dashboard
      <small>
        Hey ${account.firstname} ${account.lastname}, Welcome back!
      </small>
    </h3>
    
    <div class="row">
      <div class="col-lg-3">
        <!-- START widget-->
        <div class="panel widget">
           <div class="row row-table row-flush">
              <div class="col-xs-4 bg-green text-center">
                 <em class="fa fa-share-alt fa-2x"></em>
              </div>
              <div class="col-xs-8">
                 <div class="panel-body text-center">
                    <h4 class="mt0">-</h4>
                    <p class="mb0 text-muted">
                        ASSETS
                    </p>
                 </div>
              </div>
           </div>
        </div>
        <!-- END widget-->
      </div>
      <div class="col-lg-3">
        <!-- START widget-->
        <div class="panel widget">
           <div class="row row-table row-flush">
              <div class="col-xs-4 bg-danger text-center">
                 <em class="fa fa-users fa-2x"></em>
              </div>
              <div class="col-xs-8">
                 <div class="panel-body text-center">
                    <h4 class="mt0">-</h4>
                    <p class="mb0 text-muted">
                        USERS
                    </p>
                 </div>
              </div>
           </div>
        </div>
        <!-- END widget-->
      </div>
      <div class="col-lg-3">
        <!-- START widget-->
        <div class="panel widget">
           <div class="row row-table row-flush">
              <div class="col-xs-4 bg-info text-center">
                 <em class="fa fa-clock-o fa-2x"></em>
              </div>
              <div class="col-xs-8">
                 <div class="panel-body text-center">
                    <h4 class="mt0">-</h4>
                    <p class="mb0 text-muted">ACTIVE BOOKINGS</p>
                 </div>
              </div>
           </div>
        </div>
        <!-- END widget-->
      </div>
      <div class="col-lg-3">
        <!-- START widget-->
        <div class="panel widget">
           <div class="row row-table row-flush">
              <div class="col-xs-4 bg-inverse text-center">
                 <em class="fa fa-code-fork fa-2x"></em>
              </div>
              <div class="col-xs-8">
                 <div class="panel-body text-center">
                    <h4 class="mt0">99%</h4>
                    <p class="mb0 text-muted">Uptime</p>
                 </div>
              </div>
           </div>
        </div>
        <!-- END widget-->
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">Recently created assets</div>
      </div>
      <!-- START table-responsive-->
      <div class="table-responsive panel-body bg-white">
        <table class="table">

        </table>
      </div>
     <!-- END table-responsive-->
    </div>

    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">Recently registered users</div>
      </div>
      <!-- START table-responsive-->
      <div class="table-responsive panel-body bg-white">
        <table class="table">
          <thead>
              <tr>
                 <th>
                    <strong class="text-gray-darker">
                       <span class="pl">#</span>
                    </strong>
                 </th>
                 <th>
                    <strong class="text-gray-darker">Date</strong>
                 </th>
                 <th>
                    <strong class="text-gray-darker">Email</strong>
                 </th>
                 <th>
                    <strong class="text-gray-darker">Customer</strong>
                 </th>
                 <th>
                    <strong class="text-gray-darker">
                       <span class="circle circle-default"></span>
                    </strong>
                 </th>
              </tr>
           </thead>
           <tbody>
           <c:forEach var="dAccount" items="${dashboardAccountsData}" >
              <tr>
                 <td><a href="#">${dAccount.id}</a></td>
                 <td><fmt:formatDate type="date" value="${dAccount.created}" /></td>
                 <td>${dAccount.email}</td>
                 <td>${dAccount.firstname} ${dAccount.lastname} (${dAccount.role.label})</td>
                 <td>
                    <span class="circle circle-success"></span>
                 </td>
              </tr>
           </c:forEach>
           </tbody>
        </table>
      </div>
     <!-- END table-responsive-->
    </div>
  </div>
  <!-- END Page content-->
</section>

<%@ include file="../layout/_doc_footer.jsp" %>