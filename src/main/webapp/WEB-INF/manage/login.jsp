<%@ include file="layout/_doc_header.jsp" %>

<div class="row row-table page-wrapper">
  <div class="col-sm-6 align-middle">
     <!-- START panel-->
    <div class="panel panel-dark panel-flat">
      <div class="panel-heading text-center">
        <p class="text-center mt-lg">
          <strong>SIGN IN TO CONTINUE.</strong>
        </p>
      </div>
      <div class="panel-body">
        <span style="color:red">${requestScope.errorMessage}</span>
        <form:form method="POST" action="login" autocomplete="false">

          <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" placeholder="enter email" id="email" required="true" class="form-control"/>
          </div>

          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" placeholder="enter password" id="password" required="true" class="form-control"/>
          </div>
		  <input type="hidden" name="targetUrl" value="${requestScope.targetUrl}"/>	
          <input type="submit" class="btn btn-primary" value="Login"/>
          
          <a href="signup" class="btn btn-default">
            Need an account? Sign up
          </a>

        </form:form>
      </div>
    </div>
    <!-- END panel-->
  </div>
</div>

<%@ include file="layout/_doc_footer.jsp" %>
