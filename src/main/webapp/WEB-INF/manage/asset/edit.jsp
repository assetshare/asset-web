<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<script>
  window.APP.pageData = {
    response: ${asset.config},
    fields: ${asset.form.config}
  };
</script>

<section>
  <!-- START Page content-->
  <div class="content-wrapper" ng-controller="bookingCtrl as vm"> <!-- Page content goes in this div -->
    <h3>
      Asset management
      <small>
        All assets
      </small>
    </h3>

    <div class="panel widget">
      <div class="panel-body">
        <div class="media p mt0">
           <div class="pull-left">
              <img src="${asset.getThumbnailUrl()}" alt="Image" class="media-object img-circle thumb96">
           </div>
           <div class="media-body">
              <div class="media-heading">
                 <h3 class="mt0">${asset.title}
                    <a href="/${appName}/assets/${asset.id}/delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                 </h3>
                 <ul class="list-unstyled">
                    <li class="mb-sm">
                       <em class="fa fa-user fa-fw"></em> Owned by: <a href="${contextWithAppName}/${asset.submitter.id}/edit">${asset.submitter.firstname}</a>
                    </li>

                    <li class="mb-sm">
                      <em class="fa fa-check-square-o fa-fw"></em> Approval: ${asset.autoApprove == true ? "automatic" : "requires approval"}
                    </li>
 
                    <li class="mb-sm">
                      <em class="fa fa-calendar fa-fw"></em> Availability: ${asset.availableFrom} - ${asset.availableTo}
                    </li>

                    <li class="mb-sm">
                      <em class="fa fa-clock-o fa-fw"></em> Operating: ${asset.openFrom} - ${asset.openTo}
                    </li>
                 </ul>
              </div>
           </div>
        </div>
      </div>
      <div class="panel-body bg-inverse">
        <div class="row row-table text-center">
           <div class="col-xs-4">
              <p class="m0 h3">${asset.status}</p>
              <p class="m0 text-muted">Asset Status</p>
           </div>
           <div class="col-xs-4">
              <p class="m0 h3">${asset.dailyRate == 0 ? "-" : asset.dailyRate}</p>
              <p class="m0 text-muted">Daily Rate</p>
           </div>
           <div class="col-xs-4">
              <p class="m0 h3">${asset.hourlyRate == 0 ? "-" : asset.hourlyRate}</p>
              <p class="m0 text-muted">Hourly Rate</p>
           </div>
        </div>
      </div>
    </div>

    <%@ include file="../../shared/_additional_details.jsp" %>

  </div>
</section>