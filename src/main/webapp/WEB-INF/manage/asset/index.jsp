<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section>
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->
    <h3>
      Asset management
      <small>
        All assets
      </small>
    </h3>

    <div class="panel">
      <div class="panel-body">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th>Title</th>
              <th>Submitted by</th>
              <th>Availability</th>
              <th>Operating</th>
              <th>Hourly Rate</th>
              <th>Daily Rate</th>
              <th>Booking type</th>
              <th>Auto approve</th>
            </tr>
          </thead>
          <tbody>
          	<tr>
          		<td colspan="8"><span class="error">${errorMessage}</span></td>
          	</tr>
            <c:forEach items="${assets}" var="asset">
              <tr>
                <td><a href="/${appName}/assets/${asset.id}">${asset.title}</a></td>
                <td>${asset.submitter.firstname}</td>
                <td>${asset.availableFrom} - ${asset.availableTo}</td>
                <td>${asset.openFrom} - ${asset.openTo}</td>
                <td>${asset.hourlyRate}</td>
                <td>${asset.dailyRate}</td>
                <td>${asset.bookingType}</td>
                <td>${asset.autoApprove}</td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>  