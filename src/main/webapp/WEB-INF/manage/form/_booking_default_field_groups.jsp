<div class="form-group">
  <label for="">Book for</label>
  <input ng-model="vm.defaultFields.bookingFrom" class="form-control" type="date">
</div>

<div class="form-group">
  <label for="">Book til</label>
  <input ng-model="vm.defaultFields.bookingTo" class="form-control" type="date">
</div>

<div class="form-group">
  <label for="">Message</label>
  <textarea ng-model="vm.defaultFields.message" name="" id="" class="form-control"></textarea>
</div>