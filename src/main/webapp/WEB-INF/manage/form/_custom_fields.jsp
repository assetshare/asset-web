<div class="panel">
  <div class="panel-heading">
    <div class="panel-title">
      Define custom fields

      <div class="btn-group pull-right">
        <button title="Preview form" ng-click="vm.showFormly()" class="btn btn-default"><i class="fa fa-eye"></i></button>

        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><i class="fa fa-plus"></i> Add custom field <b class="caret"></b>
        </button>

        <ul class="dropdown-menu">
          <li>
            <a href ng-click="vm.addField('input')">Small text</a>
          </li>
          <li>
            <a href ng-click="vm.addField('textarea')">Big text</a>
          </li>
          <li>
            <a href ng-click="vm.addField('checkbox')">Checkbox</a>
          </li>
          <li>
            <a href ng-click="vm.addField('fileupload')">File upload</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="panel-body">
    <p>Any fields defined here will appear below the default fields</p>
    
    <v-accordion id="custom-fields-accord" control="accordion" class="vAccordion--default">
      <v-pane id="{{ field.id }}" ng-repeat="field in vm.fields | orderBy:'data.order'" ng-if="!field.hidden">

        <v-pane-header>
          <i class="fa fa-pencil"></i> {{ field.templateOptions.label }} <em>({{ field.type }})</em>
        </v-pane-header>

        <v-pane-content>
          <div class="row">
            <div class="col-sm-12">
              <div class="pull-right">
                <a href title="move up" class="btn btn-default btn-sm" ng-click="vm.move('up', $index)"><i class="fa fa-caret-up"></i></a>
                <a href title="move down" class="btn btn-default btn-sm" ng-click="vm.move('down', $index)"><i class="fa fa-caret-down"></i></a>
                <a href title="delete field" class="btn btn-danger btn-sm" ng-click="vm.remove(field)"><i class="fa fa-trash"></i></a>
              </div>

              <div class="form-group" style="margin-top:10px;">

                <label>Label</label>

                <input ng-change="vm.autoPlaceholder(field)" type="input" placeholder="field label" class="form-control" ng-model="field.templateOptions.label">
              </div>

            </div>
          </div>
        </v-pane-content>
      </v-pane>
    </v-accordion>

  </div>
</div>