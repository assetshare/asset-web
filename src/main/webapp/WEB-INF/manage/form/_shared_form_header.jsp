<form ng-submit="vm.saveForm()" name="vm.formObj">
  <div ng-hide="vm.showPreview">
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">Basics</div>
      </div>

      <div class="panel-body">
        <div class="form-group">
          <label for="title">Form title *</label>
          <input required class="form-control" ng-model="vm.form.name" type="text" id="title" placeholder="enter form title">
        </div>

        <div class="form-group">
          <label for="desc">Description *</label>
          <textarea required class="form-control" ng-model="vm.form.subtitle" name="" id="desc" placeholder="optional description"></textarea>
        </div>
      </div>
    </div>



