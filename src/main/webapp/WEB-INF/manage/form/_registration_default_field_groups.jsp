<div class="form-group">
  <label for="title">Title</label>
  <input id="title" class="form-control" type="text" ng-model="vm.defaultFields.title" placeholder="enter title">
</div>

<div class="form-group">
  <label for="thumbnail">Thumbnail</label>
  <div
    class="btn btn-primary btn-upload"
    upload-button
    param="uploadFile"
    url="/storage/upload"
    on-success="vm.onAfterUpload(response, vm.defaultFields, 'thumbnail')"
  >Upload</div>
</div>

<div class="form-group">
  <label for=""> <input ng-model="vm.defaultFields.autoApprove" type="checkbox" id="thumbnail"> Automatically approve</label>
</div>

<div class="form-group">
  <label for="bookingType">Booking type
  <select id="bookingType" class="form-control" ng-model="vm.defaultFields.bookingType">
    <option value="">-- select booking type --</option>
    <option value="free">Free</option>
    <option value="hourly">Hourly</option>
    <option value="daily">Daily</option>
    <option value="both">Hourly &amp; Daily</option>
  </select>
</div>

<div class="form-group" ng-show="vm.defaultFields.bookingType === 'hourly' || vm.defaultFields.bookingType === 'both'">
  <label for="hourlyRate">Hourly rate</label>
  <input class="form-control" id="hourlyRate" ng-model="vm.defaultFields.hourlyRate" type="number" placeholder="enter hourly rate">
</div>

<div class="form-group" ng-show="vm.defaultFields.bookingType === 'daily' || vm.defaultFields.bookingType === 'both'">
  <label for="dailyRate">Daily rate</label>
  <input class="form-control" id="dailyRate" ng-model="vm.defaultFields.dailyRate" type="number" placeholder="enter daily rate">
</div>

<div class="form-group">
  <label>Avaliability</label>
  <br>
  From: <input ng-model="vm.defaultFields.availableFrom" class="form-control" type="date">
  To:   <input ng-model="vm.defaultFields.availableTo" class="form-control" type="date">
</div>

<div class="form-group">
  <label for="">Operating hours</label>
  <br>
  From: <input ng-model="vm.defaultFields.openFrom" class="form-control" type="number" placeholder="0-23hours">
  To:   <input ng-model="vm.defaultFields.openTo" class="form-control" type="number" placeholder="0-23hours">
</div>