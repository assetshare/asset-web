<div class="panel">
  <div class="panel-heading">
    <div class="panel-title">
    Default fields

      <a class="pull-right" href ng-click="showDefaultFields=!showDefaultFields">
        <small>
          <i class="fa" ng-class="'fa-caret-' + (showDefaultFields ? 'up': 'down')"></i>
          {{ showDefaultFields ? 'hide' : 'show' }} default fields
        </small>
      </a>
    </div>
  </div>

  <div class="panel-body" ng-show="showDefaultFields">
    <p>These fields cannot be removed or modified</p>

    <%@ include file="./_registration_default_field_groups.jsp" %>
  </div>
</div>