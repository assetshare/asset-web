<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section ng-controller="formBuilderCtrl as vm" ng-init="vm.type = 'REGISTER'">
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->


    <ul class="nav nav-tabs">
      <li><a href="${contextWithAppName}/forms/booking">Manage booking form</a></li>
      <li class="active"><a href="#">Manage registration form</a></li>
    </ul>

    <%@ include file="./_shared_form_header.jsp" %>
    <%@ include file="./_registration_default_fields.jsp" %>
    <%@ include file="./_custom_fields.jsp" %>
    <%@ include file="./_shared_form_footer.jsp" %>

  </div>
  <!-- END Page content-->
</section>

<%@ include file="../layout/_doc_footer.jsp" %>
