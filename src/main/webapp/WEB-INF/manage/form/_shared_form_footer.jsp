

    <div class="panel">
      <div class="panel-body">
        <button type="submit" ng-disabled="vm.formObj.$invalid" class="btn btn-primary">
          Save form
        </button>
      </div>
    </div>
  </div>
</form>

<!-- preview pane -->
<div class="panel" ng-if="vm.showPreview">
  <div class="panel-heading">
    <div class="panel-title">
      Previewing custom fields

      <button ng-click="vm.showPreview=false" class="btn btn-default pull-right"><i class="fa fa-pencil"></i> Back to editor</button>
    </div>

    <div class="panel-body">
      <formly-form fields="vm.formlyFields" model="vm.model">
      </formly-form>
    </div>
  </div>
</div>
