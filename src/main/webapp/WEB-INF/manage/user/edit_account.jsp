<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section>
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->
    <h3>
      ${account.firstname} ${account.lastname}

      <small>
        Edit your account
      </small>
    </h3>

    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="panel-title">Account details</div>
      </div>
      <div class="panel-body">
        <form:form method="post" modelAttribute="account" action="${contextWithAppName}/account">
          <form:hidden path="id"/></br>
			
		  <div class="form-group">
		  	<img src="${account.gravator}" width="200" height="200"/>
		  </div>	

          <div class="form-group">
            <label for="firstname">First Name</label>
            <form:input path="firstname" placeholder="enter first name" id="firstname" required="true" cssClass="form-control"/>
          </div>

          <div class="form-group">
            <label for="lastname">Last Name</label>
            <form:input path="lastname" placeholder="enter last name" id="lastname" required="true" cssClass="form-control"/>
          </div>
          
          <div class="form-group">
            <label for="password">Password</label>
            <form:password path="password" placeholder="enter password" id="password" required="true" cssClass="form-control" showPassword="true"/>
          </div>

          <div class="form-group">
            <label for="email">Email</label>
            <form:input path="email" type="email" placeholder="enter email" id="email" readonly="true" required="true" cssClass="form-control"/>
          </div>


          <input type="submit" class="btn btn-primary btn-lg" value="Update"/>

        </form:form>
      </div>
    </div>
  </div>
  <!-- END Page content-->
</section>

<%@ include file="../layout/_doc_footer.jsp" %>
