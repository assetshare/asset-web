<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section>
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->
    <h3>
      User management
      <small>
        All users
      </small>
    </h3>

		<div class="panel panel-default">
			<div class="panel-body">
				<table class="table table--striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Registered Date</th>
							<th>App Owner</th>
							<th>Active?</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${users}" >
							<tr>
								<td>
									<a href="${contextWithAppName}/${user.id}/edit">
										${user.firstname} ${user.lastname}
									</a>
								</td>
								<td>${user.email}</td>
								<td>${user.role.roleName}</td>
								<td><fmt:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${user.created}" /></td>
								<td>${user.appOwner}</td>
								<td>${user.active}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</section>