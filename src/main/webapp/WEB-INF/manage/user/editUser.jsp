<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section>
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->
    <h3>
      FIRST_NAME LAST_NAME
      <small>
        Edit account
      </small>
    </h3>

    <c:if test="${successMsg ne null}">
      <script>toastr.success("${successMsg}")</script>
    </c:if>

    <div class="panel panel-default">
      <div class="panel-body">
        <form:form method="post" modelAttribute="user" action="update">
          <form:hidden path="id"/>

          <div class="form-group">
            <label for="firstname">First Name</label>
            <form:input path="firstname" placeholder="enter first name" id="firstname" required="true" cssClass="form-control"/>
          </div>

          <div class="form-group">
            <label for="lastname">Last Name</label>
            <form:input path="lastname" placeholder="enter last name" id="lastname" required="true" cssClass="form-control"/>
          </div>
			
		  <div class="form-group">
            <label for="password">Password</label>
            <form:password path="password" placeholder="enter password"  showPassword="true" id="password" required="true" cssClass="form-control"/>
          </div>	
			
          <div class="form-group">
            <label for="email">Email</label>
            <form:input path="email" type="email" placeholder="enter email" id="email" required="true" cssClass="form-control"/>
          </div>

    		  <div class="form-group">
            <label>Role</label>
            <form:select path="role" items="${roles}" itemLabel="label" itemValue="id" cssClass="form-control">
            </form:select>
          </div>	
          
          <div class="form-group">
            <label>Registered Date</label>
            <fmt:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${user.created}" />
          </div>

    		  <div class="form-group">
            <label>
              <form:checkbox path="active"/>
              Active
            </label>
          </div>	
          <input type="submit" class="btn btn-primary btn-lg" value="Update"/>

        </form:form>

      </div>
    </div>
  </div>
  <!-- END Page content-->
</section>

<%@ include file="../layout/_doc_footer.jsp" %>