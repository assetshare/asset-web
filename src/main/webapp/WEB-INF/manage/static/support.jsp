<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<!-- Left panel - remove all left_panel includes if page has no left panel -->
<%@ include file="../layout/_left_panel_header.jsp"%>
<%@ include file="../layout/_left_panel_content_default.jsp"%>
<%@ include file="../layout/_left_panel_footer.jsp"%>
<!-- End sidpanel -->

<!-- Add style="margin: 0;" to below <section> if no left panel -->
<section>
  <!-- START Page content-->
  <div class="content-wrapper"> <!-- Page content goes in this div -->
    <h3>
      Help &amp; Support
      <small>
        
      </small>
    </h3>


    <div class="panel panel-default">
      <div class="panel-body">
        <p>For all support enqeries please contact us via</p>

        <ul class="fa-ul">
          <li><i class="fa-li fa fa-phone"></i><a href="tel:1300100200">1300 100 200</a></li>
          <li><i class="fa-li fa fa-envelope"></i><a href="email:support@assetshare.com.au">support@assetshare.com.au</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- END Page content-->
</section>

<%@ include file="../layout/_doc_footer.jsp" %>