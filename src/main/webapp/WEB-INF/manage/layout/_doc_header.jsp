<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<c:set var="contextWithAppName" value="${context}/${sessionScope.appName}"/>
<c:set var="accountId" value="${sessionScope.loggedInAccount}"/>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asset Share</title>
    <c:set var="context" value="${pageContext.request.contextPath}" />
    <c:set var="contextWithAppName" value="${context}/${sessionScope.appName}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="${context}/static/admin/main.css">\

    <script>window.APP = { clientName: '${contextWithAppName}' };</script>

    <script src="${context}/static/admin/bundle.js"></script>
  </head>
  <body>
    <div class="wrapper" ng-app="AssetAdmin">
      