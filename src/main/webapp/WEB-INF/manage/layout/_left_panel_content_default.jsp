<li class="nav-heading">
  ${sessionScope.appName} managment
</li>

<li>
  <a href="${contextWithAppName}/assets">
    <em class="fa fa-share-alt"></em>
    <span class="item-text">Assets</span>
  </a>
</li>
<li>
  <a href="${contextWithAppName}/forms/booking">
    <em class="fa fa-wpforms"></em>
    <span class="item-text">Forms</span>
  </a>
</li>
<li>
  <a href="${contextWithAppName}/users">
    <em class="fa fa-users"></em>
    <span class="item-text">Users</span>
  </a>
</li>

<li>
  <a href="javascript:alert('fix this :)')">
    <em class="fa fa-external-link"></em>
    <span class="item-text">Visit Portal</span>
  </a>
</li>

<li class="nav-heading">
  Administration
</li>

<li>
  <a href="${contextWithAppName}/account/${accountId}">
    <em class="fa fa-user"></em>
    <span class="item-text">Edit your account</span>
  </a>
</li>

<li>
  <a href="${contextWithAppName}/support">
    <em class="fa fa-question-circle"></em>
    <span class="item-text">Help &amp; Support</span>
  </a>
</li>

<li>
  <a href="${contextWithAppName}/logout">
    <em class="fa fa-power-off"></em>
    <span class="item-text">Logout</span>
  </a>
</li>