<nav role="navigation" class="navbar navbar-default navbar-top navbar-fixed-top">
  <!-- START navbar header-->
   <div class="navbar-header">
      <a href="/manage">
         <div class="brand-logo">
            <img src="${context}/images/logo.png" alt="App Logo" class="img-responsive">
         </div>
         <div class="brand-logo-collapsed">
            <img src="${context}/images/logo-single.png" alt="App Logo" class="img-responsive">
         </div>
      </a>
   </div>
   <!-- END navbar header-->
   <!-- START Nav wrapper-->
   <div class="nav-wrapper">
      <!-- START left Navbar-->
      <ul class="nav navbar-nav navbar-left">    
         <li>
            <a href="/manage">
               <i class="fa fa-home"></i>
            </a>
         </li>


         <%--
         
            commenting this out. i want it, but cant find a good use for it :)
         <!-- START Alert menu-->
         <li class="dropdown dropdown-list">
            <a href="javascript:void(0);" data-toggle="dropdown" class="dropdown-toggle" title="Notifications">
               <em class="fa fa-bell"></em>
               <div class="label label-danger">11</div>
            </a>
            <!-- START Dropdown menu-->
            <ul class="dropdown-menu">
               <li>
                  <!-- START list group-->
                  <div class="list-group">
                     <!-- list item-->
                     <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                           <div class="pull-left">
                              <em class="fa fa-twitter fa-2x text-info"></em>
                           </div>
                           <div class="media-body clearfix">
                              <p class="m0">New followers</p>
                              <p class="m0 text-muted">
                                 <small>1 new follower</small>
                              </p>
                           </div>
                        </div>
                     </a>
                     <!-- list item-->
                     <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                           <div class="pull-left">
                              <em class="fa fa-envelope fa-2x text-warning"></em>
                           </div>
                           <div class="media-body clearfix">
                              <p class="m0">New e-mails</p>
                              <p class="m0 text-muted">
                                 <small>You have 10 new emails</small>
                              </p>
                           </div>
                        </div>
                     </a>
                     <!-- list item-->
                     <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                           <div class="pull-left">
                              <em class="fa fa-tasks fa-2x text-success"></em>
                           </div>
                           <div class="media-body clearfix">
                              <p class="m0">Pending Tasks</p>
                              <p class="m0 text-muted">
                                 <small>11 pending task</small>
                              </p>
                           </div>
                        </div>
                     </a>
                     <!-- last list item-->
                     <a href="javascript:void(0);" class="list-group-item">
                        <small>More notifications</small>
                        <span class="label label-danger pull-right">14</span>
                     </a>
                  </div>
                  <!-- END list group-->
               </li>
            </ul>
            <!-- END Dropdown menu-->
         </li> -->
         <!-- END Alert menu-->

         --%>
      </ul>
      <!-- END left Navbar-->

      <!-- START Right Navbar-->
      <ul class="nav navbar-nav navbar-right">        
         <li>
            <a href="${contextWithAppName}/account/${accountId}">
              <i class="fa fa-user"></i>
            </a>
         </li>

         <li>
            <a href="${contextWithAppName}/logout">
              <i class="fa fa-power-off"></i>
            </a>
         </li>
      </ul>
      <!-- END Right Navbar-->
   </div>
   <!-- END Nav wrapper-->
</nav>