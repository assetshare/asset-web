<%@ include file="layout/_doc_header.jsp" %>

<div class="row row-table page-wrapper">
  <div class="col-sm-8 align-middle">
    <!-- START panel-->
    <div class="panel panel-dark panel-flat anim-running anim-done">
      <div class="panel-heading text-center mb-lg">
          <p class="text-center mt-lg">
            <strong>SIGNUP TO GET INSTANT ACCESS.</strong>
           </p>
      </div>
      <div class="panel-body">
				<form:form method="POST" action="signup" modelAttribute="user">
	   			  <div class="form-group">
					    <form:label path="app.name">Domain name</form:label>
					    <form:input path="app.name" placeholder="enter domain name" required="true" cssClass="form-control"/>.assetshare.com.au
	          	  </div>
	
	   			  <div class="form-group">
					    <form:label path="email">Email</form:label>
					    <form:input path="email" placeholder="enter email" required="true" cssClass="form-control"/>
	              </div>

					<div class="form-group">
					  <form:label path="password">Password</form:label>
					  <form:password path="password" placeholder="enter password" required="true" cssClass="form-control"/>
					</div>

					<div class="form-group">
					  <form:label path="firstname">First name</form:label>
					  <form:input path="firstname" placeholder="enter first name" required="true" cssClass="form-control"/>
					</div>

					<div class="form-group">
					  <form:label path="lastname">Last name</form:label>
					  <form:input path="lastname" placeholder="enter last name" required="true" cssClass="form-control"/>
					</div>

					<input type="submit" class="btn btn-primary btn-lg" value="Create account"/>
					<a href="/manage" class="btn btn-default btn-lg">Already have an account? Login here</a>
				</form:form>
      </div>
    </div>
    <!-- END panel-->
  </div>
</div>

<%@ include file="layout/_doc_footer.jsp" %>