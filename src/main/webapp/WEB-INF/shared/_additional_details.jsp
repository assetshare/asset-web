<div class="panel" ng-if="vm.additionalDetails">
  <div class="panel-body">
    Additional information
  </div>

  <div class="panel-body form-horizontal">
    <div class="form-group" ng-repeat="field in vm.additionalDetails" ng-if="!field.hidden">
      <strong class="col-sm-2"><span class="pull-right">{{ field.label }}</span></strong>
      <div class="col-sm-10" ng-if="field.type !== 'fileupload'">
        <p>{{ field.response || 'No response' }}</p>          
      </div>
      <div class="col-sm-10" ng-if="field.type == 'fileupload'">
        <a ng-href="{{field.response.fileUrl}}">Download attachment ({{ field.response.filename }})</a>
      </div>
    </div>
  </div>
</div>