<%@ include file="./layout/_doc_header.jsp"%>
<%@ include file="./layout/_header.jsp"%>

<div class="main-container">
  <div class="container">
    <div class="row">
      <div class="col-md-8 login-box">
        <div class="inner-box category-content">
          <h2 class="title-2"><i class="icon-user-add"></i> Create your account, Its free </h2>

          <div class="row">
            <div class="col-sm-12">

              <form:form class="form-horizontal" method="POST" action="/register" modelAttribute="user">
                <input type="hidden" name="as" value="consumer"/> 

                <fieldset>
                  <div class="form-group required">
                    <form:label cssClass="col-md-4 control-label" path="firstname">First name <sup>*</sup></label></form:label>

                    <div class="col-md-6">
                      <form:input path="firstname" placeholder="enter first name" required="true" cssClass="form-control input-md"/>
                    </div>
                  </div>

                  <div class="form-group required">
                    <form:label cssClass="col-md-4 control-label" path="lastname">Last name <sup>*</sup></label></form:label>

                    <div class="col-md-6">
                      <form:input path="lastname" placeholder="enter last name" required="true" cssClass="form-control input-md"/>
                    </div>
                  </div>

                  <div class="form-group required">
                    <form:label cssClass="col-md-4 control-label" path="email">Email <sup>*</sup></label></form:label>

                    <div class="col-md-6">
                      <form:input path="email" placeholder="enter email" required="true" cssClass="form-control input-md"/>
                    </div>
                  </div>

                  <div class="form-group required">
                    <form:label cssClass="col-md-4 control-label" path="password">Password <sup>*</sup></label></form:label>

                    <div class="col-md-6">
                      <form:password path="password" placeholder="enter password" required="true" cssClass="form-control input-md"/>
                    </div>
                  </div>

                  <div class="form-grup">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-8">
                      <input type="submit" class="btn btn-primary" value="Create account"/>
                      <br><br>
                    </div>
                  </div>
                </fieldset>
              </form:form>

            </div>
          </div>
        </div>
      </div>
      <!-- /.page-content -->

<%--
      <div class="col-md-4 reg-sidebar">
        <div class="reg-sidebar-inner text-center">
          <div class="promo-text-box">
            <h3><strong>Browse & </strong></h3>

            <p> Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur
              adipiscing elit. </p>
          </div>
          <div class="promo-text-box">
            <h3><strong>Create and Manage Items</strong></h3>

            <p> Nam sit amet dui vel orci venenatis ullamcorper eget in lacus.
              Praesent tristique elit pharetra magna efficitur laoreet.</p>
          </div>
        </div>
      </div>

  --%>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>

<jsp:include page="./layout/_doc_footer.jsp" />