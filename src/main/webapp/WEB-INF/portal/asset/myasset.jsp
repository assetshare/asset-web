<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<div class="main-container">
  <div class="container">

    <div class="panel widget">
      <div class="panel-heading">
        <h2 class="panel-title pull-left">
          My listings
        </h2>
        <a class="pull-right btn btn-primary btn-large" href="/assets/register">Register New Asset</a>

      </div>
      <br>
      <div class="panel-body">
        <c:forEach items="${listings}" var="asset">
        
        <div class="item-list">
          <div class="col-sm-2 no-padding photobox">
            <div class="add-image">
              <img class="thumbnail no-margin" width="100%" src="${asset.getThumbnailUrl()}" alt="img">
            </div>
          </div>
          <!--/.photobox-->

          <div class="col-sm-7 add-desc-box">
            <div class="add-details">
              <h5 class="add-title"><a href="listings/${asset.id}">${asset.title}</a></h5>
              <span class="info-row">

                <span class="date">
                  <i class="fa fa-calendar"> </i> ${asset.availableFrom} - ${asset.availableTo}
                  <br>
                  <i class="fa fa-clock-o"> ${asset.openFrom} - ${asset.openTo}</i>

                </span>
              </span>
            </div>
          </div>
          <!--/.add-desc-box-->
          <div class="col-sm-3 text-right  price-box">

            <h3 class="item-price"> 

              $ ${asset.hourlyRate != null ? asset.hourlyRate : "-"}

              <small style="display: inline-block; transform: translateY(-3px)"><span class="add-type business-ads tooltipHere" data-toggle="tooltip" data-placement="right" title="Hourly rate"> H</span></small>
            </h3>
            <h3 class="item-price"> 

              $ ${asset.dailyRate != null ? asset.dailyRate : "-"}
              <small style="display: inline-block; transform: translateY(-3px)"><span class="add-type business-ads tooltipHere" data-toggle="tooltip" data-placement="right" title="Daily rate"> D</span></small>

            </h3>
          </div>
          <!--/.add-desc-box-->
        </div>

        </c:forEach>
      </div>
    </div>

  </div>
</div> 

<%@ include file="../layout/_doc_footer.jsp"%>
