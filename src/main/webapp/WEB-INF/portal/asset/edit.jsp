<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<script>
  window.APP.pageData = {
    response: ${asset.config},
    fields: ${asset.form.config}
  };
</script>

<div class="main-container">

  <div class="container">
    <div class="row">
      <div class="panel widget">
        <div class="panel-body">
          <div class="media p mt0">
            <div class="pull-left">
              <img width="100px" src="${asset.getThumbnailUrl()}" alt="Image" class="media-object  thumb96">
            </div>
            <div class="media-body">
              <div class="media-heading">
                 <h3 class="mt0">${asset.title}
                  
                    <a href="/assets/${asset.id}/book" class="btn btn-primary pull-right">View ad</a>
                    
                 </h3>
                 <ul class="list-unstyled">
                    <li class="mb-sm">
                      <em class="fa fa-check-square-o fa-fw"></em> Approval: ${asset.autoApprove == true ? "automatic" : "requires approval"}
                    </li>
 
                    <li class="mb-sm">
                      <em class="fa fa-calendar fa-fw"></em> Operating: ${asset.availableFrom} - ${asset.availableTo}
                    </li>
                    
                    <li class="mb-sm">
                      <em class="fa fa-clock-o fa-fw"></em> Operating: ${asset.openFrom} - ${asset.openTo}
                    </li>
                 </ul>
              </div>
            </div>
          </div>

          <hr>

          <h3>Booking requests</h3>
        
          <table class="table">
            <thead>
              <td>Booked From</td>
              <td>Booked Till</td>
              <td>Message</td>
              <td>First Name</td>
              <td>Last Name</td>
              <td>Approve / Deny</td>
            </thead>
            <tbody>
              <c:forEach items="${bookings}" var="booking">
                <tr>
                  <td>${booking.bookingFrom}</td>
                  <td>${booking.bookingTo}</td>
                  <td>${booking.message}</td>
                  <td>${booking.submitter.firstname}</td>
                  <td>${booking.submitter.lastname}</td>
                  <td>
                    <c:choose>
                      <c:when test="${booking.status eq 'pending'}">
                        <a href="/booking/${booking.id}/approve">Approve</a> <a href="/booking/${booking.id}/deny">Deny</a>
                      </c:when>
                      <c:when test="${booking.status eq 'approve'}">
                        <span style="color:green">APPROVED</span>
                      </c:when>
                      <c:otherwise>
                        <span style="color:red">DENIED</span>
                      </c:otherwise>
                    </c:choose>
                    
                  </td>
                </tr>
              </c:forEach>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>





<%@ include file="../layout/_doc_footer.jsp"%>
