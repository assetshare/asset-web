<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>


<div class="main-container" ng-controller="registerCtrl as vm">
  <div class="container">
    <div class="row">
      <div class="col-md-9 page-content">
        <div class="inner-box category-content">
          <h2 class="title-2 uppercase"><strong> {{ vm.form.name}}</strong>
            <p><small>{{ vm.form.subtitle }}</small></p>
          </h2>

          <div class="col-sm-12">
            <form class="form">
        
              <fieldset>
                <%@ include file="../../manage/form/_registration_default_field_groups.jsp" %>
              </fieldset>
        
              <fieldset>
                <div ng-show="vm.form.config.length" class="content-subheading"><i class="icon-user fa"></i> <strong>Additional details</strong></div>
        
                <formly-form fields="vm.form.config" model="vm.response">
                  <button type="submit" class="btn btn-primary" ng-click="vm.submit(vm.response)">Submit</button>
                  <br><br>
                </formly-form>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <!-- /.page-content -->

      <div class="col-md-3 reg-sidebar">
        <div class="reg-sidebar-inner text-center">
          <div class="promo-text-box"><i class=" fa fa-4x fa-newspaper-o"></i>

            <h3><strong>Post your Asset</strong></h3>

            <p>Make the most of your assets. Advertise online for members to browse, book and borrow while you're not using them.</p>
          </div>

          <div class="panel sidebar-panel">
            <div class="panel-heading uppercase">
              <small><strong>How to post a good Ad?</strong></small>
            </div>
            <div class="panel-content">
              <div class="panel-body text-left">
                <ul class="fa-ul">
                  <li><i class="fa-li fa fa-check"></i>Use a brief &amp; descriptive title</li>
                  <li><i class="fa-li fa fa-check"></i>Add a nice photo</li>
                  <li><i class="fa-li fa fa-check"></i>Set a reasonable price</li>
                  <li><i class="fa-li fa fa-check"></i>Check your spelling</li>
                </ul>
              </div>
            </div>
          </div>


        </div>
      </div>
      <!--/.reg-sidebar-->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>

<%@ include file="../layout/_doc_footer.jsp"%>
