
<div class="intro">
  <div class="dtable hw100">
    <div class="dtable-cell hw100">
      <div class="container text-center">
        <h1 class="intro-title animated fadeInDown">Find assets near you</h1>

        <p class="sub animateme fittext3 animated fadeIn">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <div class="row search-row animated fadeInUp">
          <form method="get" action="/search">
            <div class="col-lg-8 col-sm-8 search-col relative"><i class="fa fa-search fa-fw icon-append"></i>
              <input type="text" name="searchString" class="form-control has-icon"
                   placeholder="search by keyword ..." value="">
            </div>
            <div class="col-lg-4 col-sm-4 search-col">
              <button class="btn btn-primary btn-search btn-block"><i
                  class="icon-search"></i><strong>Find</strong></button>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>


<!-- /.intro -->

<div class="main-container">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 page-content col-thin-right">
        <div class="inner-box relative">
          <h2 class="title-2">Recently shared</h2>    

          <table class="table table-striped">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>Title</th>
                <th>Availability</th>
                <th>Operating</th>
              </tr>
            </thead>
            <tbody>
              <!-- for(asset in getRecentListings(5)) { -->
                <tr>
                  <td>{asset.thubmnail}</td>
                  <td>{asset.titile}</td>
                  <td>{asset.availableFrom} - {asset.availableTo}</td>
                  <td>{asset.availableFrom} - {asset.availableTo}</td>
                </tr>
              <!-- } -->
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.main-container -->

<%--
<div class="page-info hasOverly" style="background: url(/static/user/images/bg.jpg); background-size:cover">
  <div class="bg-overly ">
    <div class="container text-center section-promo">
      <div class="row">
        <div class="col-sm-3 col-xs-6 col-xxs-12">
          <div class="iconbox-wrap">
            <div class="iconbox">
              <div class="iconbox-wrap-icon">
                <i class="icon  icon-group"></i>
              </div>
              <div class="iconbox-wrap-content">
                <h5><span>2200</span></h5>

                <div class="iconbox-wrap-text">Trusted Seller</div>
              </div>
            </div>
            <!-- /..iconbox -->
          </div>
          <!--/.iconbox-wrap-->
        </div>

        <div class="col-sm-3 col-xs-6 col-xxs-12">
          <div class="iconbox-wrap">
            <div class="iconbox">
              <div class="iconbox-wrap-icon">
                <i class="icon  icon-th-large-1"></i>
              </div>
              <div class="iconbox-wrap-content">
                <h5><span>100</span></h5>

                <div class="iconbox-wrap-text">Categories</div>
              </div>
            </div>
            <!-- /..iconbox -->
          </div>
          <!--/.iconbox-wrap-->
        </div>

        <div class="col-sm-3 col-xs-6  col-xxs-12">
          <div class="iconbox-wrap">
            <div class="iconbox">
              <div class="iconbox-wrap-icon">
                <i class="icon  icon-map"></i>
              </div>
              <div class="iconbox-wrap-content">
                <h5><span>700</span></h5>

                <div class="iconbox-wrap-text">Locations</div>
              </div>
            </div>
            <!-- /..iconbox -->
          </div>
          <!--/.iconbox-wrap-->
        </div>

        <div class="col-sm-3 col-xs-6 col-xxs-12">
          <div class="iconbox-wrap">
            <div class="iconbox">
              <div class="iconbox-wrap-icon">
                <i class="icon icon-facebook"></i>
              </div>
              <div class="iconbox-wrap-content">
                <h5><span>50,000</span></h5>

                <div class="iconbox-wrap-text"> Facebook Fans</div>
              </div>
            </div>
            <!-- /..iconbox -->
          </div>
          <!--/.iconbox-wrap-->
        </div>

      </div>

    </div>
  </div>
</div>
--%>
<!-- /.page-info -->
