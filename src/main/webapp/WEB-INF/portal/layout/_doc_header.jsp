<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<c:set var="contextWithAppName" value="${context}/${sessionScope.appName}"/>
<c:set var="accountId" value="${sessionScope.loggedInAccount}"/>
<c:set var="appName" value="${sessionScope.appName}"/>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="assets/ico/favicon.png">
  <title>APP NAME</title>
  <!-- Main css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <link href="${context}/static/user/main.css" rel="stylesheet">

  <script>window.APP = {};</script>


  <!-- Just for debugging purposes. -->
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script src="${context}/static/user/bundle.js"></script>
</head>
<body>
<div id="wrapper" ng-app="AssetPortal">