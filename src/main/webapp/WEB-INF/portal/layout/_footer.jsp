<div class="footer" id="footer">
  <div class="container">
    <ul class=" pull-left navbar-link footer-nav">
      <li><a href="index.html"> Home </a> <a href="about-us.html"> About us </a> <a href="terms-conditions.html"> Terms and 
        Conditions </a> <a href="#"> Privacy Policy </a> <a href="contact.html"> Contact us </a>
    </ul>
    <ul class="pull-right navbar-link footer-nav">
      <li> &copy; {Date().getYear()} AssetShare</li>
    </ul>
  </div>

</div>
<!-- /.footer -->