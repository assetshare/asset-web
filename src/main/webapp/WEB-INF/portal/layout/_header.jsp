<div class="header">
  <nav class="navbar navbar-site navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
            class="icon-bar"></span> <span class="icon-bar"></span></button>

        <a href="/" class="navbar-brand logo logo-title">
          <img src="/static/user/images/logo-placeholder.jpg" height="100%" alt="">
        </a></div>
      <div class="navbar-collapse collapse">

        <ul class="nav navbar-nav navbar-right">
          <c:choose>
	          <c:when test="${empty accountId || accountId == 0}">
		      	<li><a href="/portal/login">Login</a></li>
		      	<li><a href="/portal/register?as=user">Signup</a></li>
	          </c:when>
	          <c:otherwise>
	          	<li><a href="/listings">My Assets</a></li>
	          	<li><a href="/${accountId}/bookings">My Bookings</a></li>
	          	<li><a href="/logout"><i class="fa fa-power-off"></i></a></li>
	          </c:otherwise>
          </c:choose>
          
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>

</div>

<!-- /.header -->
<c:if test="${not empty sessionScope.message || not empty sessionScope.errorMessage }">
  <br>
  <div class="container">
    <c:if test="${not empty sessionScope.message}"> 
        <div class="alert alert-success">${sessionScope.message}</div>
        <%session.removeAttribute("message"); %>
      </c:if>
      <c:if test="${not empty sessionScope.errorMessage }">
        <div class="alert alert-danger">${sessionScope.errorMessage}</div>
        <%session.removeAttribute("errorMessage"); %>
      </c:if> 
  </div>
</c:if>