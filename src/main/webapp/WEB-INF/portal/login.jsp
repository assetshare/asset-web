<%@ include file="./layout/_doc_header.jsp"%>
<%@ include file="./layout/_header.jsp"%>

<div class="main-container">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 login-box">
        <div class="panel panel-default">
          <div class="panel-body">
            <span style="color:red">${requestScope.errorMessage}</span>
            <form:form method="POST" action="/login" autocomplete="false">
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" name="email" placeholder="enter email" id="email" required="true" class="form-control"/>
              </div>

              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" placeholder="enter password" id="password" required="true" class="form-control"/>
              </div>

              <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" value="Login"/>
              </div>
            </form:form>
          </div>
          <div class="panel-footer">
            <p class="text-center"><a href="forgot-password.html"> Lost your password? </a>
            </p>
          </div>
        </div>
        <div class="login-box-btm text-center">
          <p> Don't have an account? <br>
            <a href="/portal/register"><strong>Sign Up !</strong> </a></p>
        </div>
      </div>
    </div>
  </div>
</div>

<jsp:include page="./layout/_doc_footer.jsp" />
