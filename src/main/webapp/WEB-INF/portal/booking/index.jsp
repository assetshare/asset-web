<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>


<script>
  window.APP.pageData = {
    fields: ${bookingForm.config},
    responses: ${asset.config}
  };
</script>

<div class="main-container" ng-controller="bookingCtrl as vm">
  <div class="container">
    <div class="row">
      <div class="panel widget">
        <div class="panel-body">
          <div class="media p mt0">
             <div class="pull-left">
                <img width="100px" src="${asset.getThumbnailUrl()}" alt="Image" class="media-object  thumb96">
             </div>
             <div class="media-body">
                <div class="media-heading">
                   <h3 class="mt0">${asset.title}
                    
                    <c:choose>
                      <c:when test="${empty accountId || accountId == 0}">
                        <a href="/portal/login" class="btn btn-primary pull-right">Login to book</a>
                      </c:when>
                      <c:when test="${accountId == asset.submitter.id}">
                        <a href="/listings/${asset.id}" class="btn btn-primary pull-right">Manage</a>
                      </c:when>
                      <c:otherwise>
                        <a href class="btn btn-primary pull-right" ng-click="vm.openBookingForm()">Book</a>
                      </c:otherwise>
                    </c:choose>

                   </h3>
                   <ul class="list-unstyled">
                      <li class="mb-sm">
                         <em class="fa fa-user fa-fw"></em> Owned by: ${asset.submitter.firstname} ${asset.submitter.lastname}
                      </li>

                      <li class="mb-sm">
                        <em class="fa fa-check-square-o fa-fw"></em> Approval: ${asset.autoApprove == true ? "automatic" : "requires approval"}
                      </li>
   
                      <li class="mb-sm">
                        <em class="fa fa-calendar fa-fw"></em> Operating: ${asset.availableFrom} - ${asset.availableTo}
                      </li>
                      
                      <li class="mb-sm">
                        <em class="fa fa-clock-o fa-fw"></em> Operating: ${asset.openFrom} - ${asset.openTo}
                      </li>
                   </ul>
                </div>
             </div>
          </div>
        </div>
        <div class="panel-body bg-inverse">
          <div class="row row-table text-center">
             <div class="col-xs-4">
                <p class="m0 h3">${asset.status}</p>
                <p class="m0 text-muted">Asset Status</p>
             </div>
             <div class="col-xs-4">
                <p class="m0 h3">${asset.dailyRate == 0 ? "-" : asset.dailyRate}</p>
                <p class="m0 text-muted">Daily Rate</p>
             </div>
             <div class="col-xs-4">
                <p class="m0 h3">${asset.hourlyRate == 0 ? "-" : asset.hourlyRate}</p>
                <p class="m0 text-muted">Hourly Rate</p>
             </div>
          </div>
        </div>
      </div>

      <%@ include file="../../shared/_additional_details.jsp" %>

      <div id="book" class="panel" ng-show="vm.showBookingForm">
        <div class="panel-heading">
          <div class="panel-title">
            {{ vm.form.name }}
          </div>
          <p>{{ vm.form.subtitle }}</p>
        </div>

        <div class="panel-body">
          <form class="form">
            <fieldset>
              <legend>Booking information</legend>
              <%@ include file="../../manage/form/_booking_default_field_groups.jsp" %>
            </fieldset>

            <fieldset>
              <legend>Additional details</legend>

              <formly-form fields="vm.form.config" model="vm.response">
                <button type="submit" class="btn btn-primary" ng-click="vm.submit(vm.response, ${asset.id})">Submit</button>
              </formly-form>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.main-container -->

<%@ include file="../layout/_doc_footer.jsp" %>
