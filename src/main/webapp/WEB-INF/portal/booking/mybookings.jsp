<%@ include file="../layout/_doc_header.jsp"%>
<%@ include file="../layout/_header.jsp" %>

<div class="main-container">

  <div class="container">
    <div class="panel widget">
			<div class="panel-heading">
				<h2 class="panel-title">
					My Bookings
				</h2>
				
			</div>

			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<td></td>
						<td>Asset Title</td>
						<td>Booked From</td>
						<td>Booked Till</td>
						<td>Message</td>
						<td>Status</td>
					</thead>
					<tbody>
						<c:forEach items="${bookings}" var="booking">
							<tr>
								<td><img src="${booking.asset.getThumbnailUrl()}" width="100" height="100"/></td>
								<td><a href="/assets/${booking.asset.id}/book">${booking.asset.title}</a></td>
								<td>${booking.bookingFrom}</td>
								<td>${booking.bookingTo}</td>
								<td>${booking.message}</td>
								<td>
									<c:choose>
										<c:when test="${booking.status eq 'approve'}">
											<span style="color:green">APPROVED</span>
										</c:when>
										<c:when test="${booking.status eq 'pending'}">
											<span style="color:blue">PENDING</span>
										</c:when>
										<c:otherwise>
											<span style="color:red">DENIED</span>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
						
					</tbody>
				</table>
			</div>
    </div>

  </div>
</div>

<%@ include file="../layout/_doc_footer.jsp" %>