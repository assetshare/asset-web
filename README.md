# README #

To start the server RUN below command from asset-web

mvn spring-boot:run


To access the application,

http://localhost:8080


#DATABASE#

Install MariaDB on Mac

https://mariadb.com/blog/installing-mariadb-10010-mac-os-x-homebrew

After you install Mariadb, copy the scripts from scripts.sql and run in it from Mariadb command line tool.

Once done, start the server and you should be able to signup and later login